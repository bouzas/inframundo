# -*- encoding: utf-8 -*-

import pygame
from game.director import Director
from game.escena.menu import Menu

if __name__ == '__main__':
    # Inicializamos la libreria de pygame
    pygame.init()
    # Creamos el director
    director = Director()
    # Creamos la escena con la pantalla inicial
    escena = Menu(director, "main_menu")
    # Le decimos al director que apile esta escena
    director.apilarEscena(escena)
    # Y ejecutamos el juego
    director.ejecutar()
    # Cuando se termine la ejecucion, finaliza la libreria
    pygame.quit()