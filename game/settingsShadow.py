# -*- encoding: utf-8 -*-
import shelve
class Settings():
    def __init__(self, paramsFile='settings.txt'):
        self.paramsFile = paramsFile
        self.data = shelve.open(paramsFile)
    def getParam(self, param):
        return self.data[param]
    def setParam(self, param, value):
        data = self.data
        data[param] = value
        self.data = data
        data.close
