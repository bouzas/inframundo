# -*- coding: utf-8 -*-

import os, game.escena.mapas.mapas as mapas
from game.gestorRecursos import GestorRecursos
from game.settings.settings import Settings
from game.settings.text_settings import *

POS = (100,100)
LB_ROJA = os.path.join("misc","lifebar_roja.png")
LB_GRIS = os.path.join("misc","lifebar_gris.png")
SB_VERDE = os.path.join("misc","staminabar_verde.png")
SB_GRIS = os.path.join("misc","staminabar_gris.png")

class Hud(pygame.sprite.Sprite):

	def __init__ (self, personaje, num_fase):
		pygame.sprite.Sprite.__init__(self);
		
		self.lb_roja = GestorRecursos.CargarImagen(LB_ROJA)
		self.lb_gris = GestorRecursos.CargarImagen(LB_GRIS)
		self.sb_verde = GestorRecursos.CargarImagen(SB_VERDE)
		self.sb_gris = GestorRecursos.CargarImagen(SB_GRIS)
		
		self.personaje = personaje
		
		ori_rect = self.lb_roja.get_rect()
		self.drawn_area_lb = ori_rect.copy() # copia del rect original a la que le vamos cambiando el tamaño
		self.bottom_coord_lb = ori_rect.bottom
		self.full_height_lb = ori_rect.height
		self.alignment_lb = 0
		
		ori_rect = self.sb_verde.get_rect()
		self.drawn_area_sb = ori_rect.copy() # copia del rect original a la que le vamos cambiando el tamaño
		self.bottom_coord_sb = ori_rect.bottom
		self.full_height_sb = ori_rect.height
		self.alignment_sb = 0

		self.font = TEXT_TYPES[3]
		self.white = Settings().getParam("WHITE") # ?!?!??!!
		self.red = Settings().getParam("RED") # ??????!!
		self.color_stamina = self.white
		self.color_vida = self.white
		self.text_box_stamina = self.font.render("100%",0,self.color_stamina)
		self.text_box_vida = self.font.render("100%",0,self.color_vida)
		self.text_box_semilla = self.font.render(str(mapas.generator.semilla),0,self.white)

		self.ataque_level = self.font.render("ATK: ",0, self.white)
		self.ataque = self.font.render(str(self.personaje.ataque),0,self.white)
		if num_fase > 5:
			self.num_fase = self.font.render("Nivel Final",0,self.white)
		else:
			self.num_fase = self.font.render("Nivel " + str(num_fase+1),0,self.white)

		self.osiris_parts = []

	def update(self, tiempo):
		ratio_vida = 1.0
		ratio_stamina = 1.0

		self.osiris_parts = self.personaje.get_osiris_parts_collected()
		self.ataque = self.font.render(str(self.personaje.ataque),0,self.white)
		
		if self.personaje.vidaBase > 0:
			if self.personaje.vida >= 0:
				ratio_vida = self.personaje.vida / float(self.personaje.vidaBase)
			else:
				ratio_vida = 0.0

		if self.personaje.staminaBase > 0:
			if self.personaje.stamina >= 0:
				ratio_stamina = self.personaje.stamina / float(self.personaje.staminaBase)
			else:
				ration_stamina = 0.0 

		self.drawn_area_lb.height = int(self.full_height_lb * ratio_vida) # lo cortamos a medida
		self.drawn_area_lb.bottom = self.bottom_coord_lb # lo alineamos abajo internamente
		self.alignment_lb = self.full_height_lb - self.drawn_area_lb.height # y lo alineamos abajo externamente

		self.drawn_area_sb.height = int(self.full_height_sb * ratio_stamina) # lo cortamos a medida
		self.drawn_area_sb.bottom = self.bottom_coord_sb # lo alineamos abajo internamente
		self.alignment_sb = self.full_height_sb - self.drawn_area_sb.height # y lo alineamos abajo externamente
		
		self.text_box_stamina = self.font.render(str(int(ratio_stamina * 100)) + "%",0,self.color_stamina)
		self.text_box_vida = self.font.render(str(int(ratio_vida * 100)) + "%",0,self.color_vida)

	def dibujar(self, screen):
		posx, posy = POS
		# barra de vida
		screen.blit_abs(self.lb_gris,POS,side="bottomleft")
		screen.blit_abs(self.lb_roja, (posx,posy-self.alignment_lb), area=self.drawn_area_lb,side="bottomleft")
		# barra de vida
		screen.blit_abs(self.sb_gris,POS,side="bottomright")
		screen.blit_abs(self.sb_verde, (posx,posy-self.alignment_sb), area=self.drawn_area_sb,side="bottomright")
		# texto stamina
		screen.blit_abs(self.text_box_stamina,(100,0),side="bottomright")
		screen.blit_abs(self.text_box_vida,(100,0),side="bottomleft")
		screen.blit_abs(self.text_box_semilla,(50,50),side="topright")
		cont = 500
		for i,element in enumerate(self.osiris_parts):
			cont +=  element.get_rect().width*(i+1)
			screen.blit_abs(element,(cont,10), side="topleft")

		screen.blit_abs(self.ataque_level, (50, 200), side="topleft")
		screen.blit_abs(self.ataque, (50 + self.ataque_level.get_rect().width, 200), side="topleft")
		screen.blit_abs(self.num_fase, (50,100), side="topleft")

