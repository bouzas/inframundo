# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *
from game.sound import MyMixer

class Trigger(pygame.sprite.Sprite):
	def __init__(self, rect):
		super(Trigger, self).__init__()
		self.image = pygame.Surface(rect.size)
		self.image.fill((0,255,255))
		self.rect = rect

	def trigger(self, sprite, interacts):
		pass # Sobreescribir este método para la acción del trigger cuando lo activa un sprite

class Teleporter(Trigger):
	def __init__(self, rect):
		super(Teleporter, self).__init__(rect)
		self.dest = (0,0)
		self.facing = [] # una lista de los estados en los que el sprite puede teleportarse
		# NOTAS: Facing es una guarrada, fruto del acople entre la dirección que mira el personaje y su número de orden en la hoja de sprites
		# si se desacoplase, podríamos tener un facing que correspondiese con la dirección en la que queremos que esté mirando el personaje para poder atravesar el portal
		# y además sería genérico para todos los personajes y no solo para el jugador. También cantaríamos todos en coro "Un mundo ideal"
                self.mixer = MyMixer()
                self.sonidoPuerta = self.mixer.getSound("door.wav")
                
	def trigger(self, fase, sprite, interacts):
		if interacts and sprite.estado in self.facing:
                        self.sonidoPuerta.play()
			sprite.rect.center = self.dest

class LevelEnd(Trigger):
	def __init__(self,rect):
		super(LevelEnd, self).__init__(rect)
                self.mixer = MyMixer()
                self.sonidoTeleport = self.mixer.getSound("portal.wav")

	def trigger(self, fase, sprite, interacts):
                self.sonidoTeleport.play()
		fase.next_level()
