# -*- coding: utf-8 -*-

from game.sprite.base import *
from game.sprite.items import *
from game.settings.settings import Settings
from game.settings.sprite_settings import *
from game.sound import MyMixer
"Clase genérica Personaje. Los enemigos y el jugador son personajes"

class Personaje(Movil):

    def __init__ (self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados, vidaBase, ataque):
        from game.escena.mapas.mapas import Generador
        Movil.__init__(self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados);

        #todos los objetos tienen el generador de aleatorios
        self.rand = Generador.instancia()
        
        #El movimiento es una tupla de valores. Cada valor puede ser -1, 0 y 1 y representa el movimiento en un eje (vertical y horizontal)
        self.movimiento = (0,0) #quieto

        #datos comunes a todos los personajes
        self.vidaBase = vidaBase
        self.vida = vidaBase
        self.ataque = ataque
        self.dead = False
        self.mixer = MyMixer()
        self.sonidoWilhelmScream = self.mixer.getSound("Wilhelm_Scream.ogg")

    def quitarVida(self, cantidad):
        self.vida -= cantidad
        if self.vida <= 0:
            self.sonidoWilhelmScream.play()
            self.dead = True
        #if self.estado == self.estados['ABAJO']:
            #saltar arriba
            #self.establecerPosicion((self.rect.center[0],self.rect.center[1]-REBOTE))
        #elif self.estado == self.estados['ARRIBA']:
            #saltar abajo
            #self.establecerPosicion((self.rect.center[0],self.rect.center[1]+REBOTE))
        #if self.estado == self.estados['DERECHA']:
            #saltar izquierda
            #self.establecerPosicion((self.rect.center[0]-REBOTE,self.rect.center[1]))
        #elif self.estado == self.estados['IZQUIERDA']:
            #saltar derecha
            #self.establecerPosicion((self.rect.center[0]+REBOTE,self.rect.center[1]))
        return self.dead

    def curarVida(self, cantidad):
        self.vida += cantidad
        if self.vida > self.vidaBase:
            self.vida = self.vidaBase

    def fijarVida(self, porcentaje):
        """ Porcentaje es un número entre 0 y 1 que representa
        la cantidad de la vida base a establecerle al personaje"""
        self.vida = int(self.vidaBase * porcentaje)
        if self.vida <= 0:
            self.dead = True

#------------------------------------------------------------#

"Todos los enemigos comunes con los que el jugador debe combatir heredan de esta clase"

class Enemigo(Personaje):

    def __init__(self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados, vidaBase, ataque):
        Personaje.__init__(self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados, vidaBase, ataque);
        self.habitacion = None
        self.prob = 20
        self.drop = self.rand.generarNumRand(100) < self.prob
        self.retardoInmunidadOriginal = 75
        self.retardoInmunidad = 0

    def establecer_habitacion(self, habitacion):
        self.habitacion = habitacion

    def mover_cpu(self, grupoParedes, jugador, current_room):
        # Por defecto un enemigo no hace nada
        pass

    def conflicto(self, jugador):
        if jugador.atacando and self.retardoInmunidad <= 0:
            self.quitarVida(jugador.ataque)
            self.retardoInmunidad = self.retardoInmunidadOriginal

    def recompensa(self):
        item = None
        if self.drop:
            item = ItemAtaque()
            item.establecerPosicion(self.rect.center)
        return item

    def detect_jugador(self, jugador):
        mirandoPrevia = self.estado
        
        miX, miY = self.rect.center
        jugX, jugY = jugador.rect.center
        valorX = miX - jugX
        valorY = miY - jugY

        pos_relativaX = 0
        pos_relativaY = 0

        if abs(valorX) > jugador.rect.width/2:
            if valorX > 0:
                pos_relativaX = -1
            elif valorX < 0:
                pos_relativaX = 1
        if abs(valorY) > jugador.rect.height/2:
            if valorY > 0:
                pos_relativaY = -1
            elif valorY < 0:
                pos_relativaY = 1

        if pos_relativaY == -1:
            self.movimiento = (0,-1)
            self.estado = self.estados['ARRIBA']
        elif pos_relativaY == 1:
            self.movimiento = (0,1)
            self.estado = self.estados['ABAJO']
        elif pos_relativaX == 1:
            self.movimiento = (1,0)
            self.estado = self.estados['DERECHA']
        elif pos_relativaX == -1:
            self.movimiento = (-1,0)
            self.estado = self.estados['IZQUIERDA']
        self.actualizarPostura(self.estado)
        
        if abs(pos_relativaX) == abs(pos_relativaY):
            return (pos_relativaX*0.707, pos_relativaY*0.707)
        else:
            return (pos_relativaX, pos_relativaY)

#-----------------------------------#

"Todos los dioses, que son un tipo distinto de enemigo, heredan de esta clase"

# class Dios(Personaje): 
