# -*- coding: utf-8 -*-

from game.sprite.base import *
from game.gestorRecursos import GestorRecursos
from game.settings import *


ESTADOS = {'PARCIAL':0,'TOTAL':1,'USADO':2}

class Vida(Animado):

    def __init__ (self, archivoImagen, archivoCoordenadas, numImages, retardoAnimacion, estados):
        Animado.__init__(self, archivoImagen, archivoCoordenadas, numImages, retardoAnimacion, estados);
        
    def curarVida(self, personaje):
        pass

class CuracionParcial(Vida):
    def __init__(self):
        Vida.__init__(self,'estaticos/Orbes.png','estaticos/Orbes_coords.txt',[1, 1, 1],1,ESTADOS)
        self.vida = 25
        self.estado = self.estados['PARCIAL']

    def curarVida(self, personaje):
        if(self.estado!=self.estados['USADO']):
            if (personaje.vidaBase-personaje.vida) >= self.vida:
                personaje.vida+=self.vida
                self.actualizarPostura(self.estados['USADO'])
            else:
                personaje.vida = personaje.vidaBase
                self.actualizarPostura(self.estados['USADO'])

class CuracionTotal(Vida):
    def __init__(self):
        Vida.__init__(self,'estaticos/Orbes.png','estaticos/Orbes_coords.txt',[1, 1, 1],1,ESTADOS)
        self.actualizarPostura(self.estados['TOTAL'])
        self.funcionando=self.estados['TOTAL']

    def curarVida(self, personaje):
        if(self.estado!=self.estados['USADO']):
            if personaje.vida<personaje.vidaBase:
                personaje.vida = personaje.vidaBase
                self.actualizarPostura(self.estados['USADO'])

