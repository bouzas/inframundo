# -*- coding: utf-8 -*-

import random, math
from game.sprite.personajes import *

"Esta clase incluye todos los tipos de enemigos que heredan de la clase Enemigo"

class Murcielago(Enemigo):
	DIRECCIONES = [(-1,1),(1,-1),(1,1),(-1,-1)]
	MIRANDO = [2,3,3,2] # derecha o izquierda
	def __init__(self):
		ESTADOS = {'ARRIBA':0,'ABAJO':1,'IZQUIERDA':2,'DERECHA':3}
		self.velOriginal = 0.3
		self.retardoOriginal = 5
		Enemigo.__init__(self,'personajes/silverbat.png','personajes/silverbat_coords.txt', [4, 4, 4, 4], self.retardoOriginal, ESTADOS, 20, 1);
		self.direccion = self.rand.generarNumRand(3)
		self.incremento = 1 if self.rand.generarNumRand(1) else -1
		self.velocidad = Murcielago.DIRECCIONES[self.direccion]

	def mover_cpu(self, jugador, current_room):

		if self.collision:
			self.direccion = (self.direccion + self.incremento) % 4 # cambiamos de dirección
			dirx, diry = Murcielago.DIRECCIONES[self.direccion] # actualizamos la velocidad
			self.velocidad = dirx*self.velOriginal, diry*self.velOriginal

		self.actualizarPostura(Murcielago.MIRANDO[self.direccion])

class Thief(Enemigo):
	def __init__(self):
		ESTADOS = {'ABAJO':0,'IZQUIERDA':1,'DERECHA':2,'ARRIBA':3}
		self.velOriginal = 0.4
		self.velHuida = 0.75
		self.retardoOriginal = 5
		# Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
		Enemigo.__init__(self,'personajes/thief.png','personajes/thief.coords', [4, 4, 4, 4], self.retardoOriginal, ESTADOS, 40, 20);
		self.screen_height = Settings().getParam("SCREEN_HEIGHT")
		self.screen_width = Settings().getParam("SCREEN_WIDTH")
		
		#cuando ha tocado al enemigo, sale huyendo en otra dirección y desaparece
		self.touched = False
		self.hitbox = self.rect.inflate((-30,-30))

	def mover_cpu(self, jugador, current_room):
		mirandoPrevia = self.estado
		miX, miY = self.rect.center

		if self.retardoInmunidad > 0:
			self.retardoInmunidad -= 1
			self.velocidad = (0,0)

		else:
			if not self.touched:
				posiX, posiY = jugador.rect.center
				distancia = math.sqrt(pow(abs(miX-posiX),2) + pow(abs(miY-posiY),2))

				if distancia <= 400:
					self.incrementoX, self.incrementoY = self.detect_jugador(jugador)
					self.velocidad = (self.incrementoX * self.velOriginal, self.incrementoY * self.velOriginal)
			else:
				if miX < self.screen_height or miX > 0 or miY < self.screen_width or miY > 0:
					self.velocidad = (self.incrementoX * self.velHuida, self.incrementoY * self.velHuida)
				else:
					self.dead = True

	
	#Cuando ha tocado al enemigo por primera vez, huye y desaparece
	def conflicto(self, jugador):
		if jugador.atacando and self.retardoInmunidad <= 0:
			self.quitarVida(jugador.ataque)
			self.retardoInmunidad = self.retardoInmunidadOriginal

		if not self.touched: 
			self.solido = False
			self.incrementoX = (self.incrementoX + 2 % 3) - 1
			self.incrementoY = (self.incrementoY + 2 % 3) - 1
			self.touched = True
