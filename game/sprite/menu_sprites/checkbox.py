# -*- coding: utf-8 -*-

from game.sprite.miSprite import MiSprite
from game.gestorRecursos import GestorRecursos
from game.settings.settings import Settings

checkbox = "menus/checkbox.png"
#----------------------------------------------#

class SingleBox(MiSprite):
    def __init__(self,text,images,rects):
        MiSprite.__init__(self)
        self.images = images
        self.rects = rects
        self.posicion = (0,0)
        self.scroll = (0,0)

        #current image and rect
        self.image = images[0]
        self.rect = rects[0]
        self.text = text
        self.checked = True

    def dibujar(self,screen):
        screen.blit(self.image, self.rect.topleft)

    def check(self):
        self.checked = not self.checked
        if self.checked:
            self.image = self.images[0]
        else:
            self.image = self.images[1]

class CheckBox(object):
    def __init__(self, x, y, font):
        self.num_items = num_items
        sheet = GestorRecursos.CargarImagen(checkbox, -1).convert_alpha()
        self.sheet = pygame.transform.scale(sheet, Settings().getparam("SCREEN_WIDTH"), Settings().getparam("SCREEN_HEIGHT"))
        self.images = [self.image_at((0,0,50,50)), self.image_at((50,0,100,50))]
        self.rects = [self.images[0].get_rect(), self.images[1].get_rect()]

        self.array = SingleBox(text,self.images,self.rects)

    # Load a specific image from a specific rectangle
    def image_at(self, rectangle, colorkey = None):
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size).convert()
        image.blit(self.sheet, (0, 0), rect)
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0,0))
            image.set_colorkey(colorkey, pygame.RLEACCEL)
        return image

    def establecerPosicion(self, posicion):
        x,y = posicion
        self.array.establecerPosicion((x,y+(self.array.rect.height/2)))

    def move(self, posicion):
        return 

    def dibujar(self,screen):
        self.array.dibujar(screen)

    def is_mouse_selection(self, (posx, posy)):
        if (posx >= self.array.rect.left and posx <= self.array.rect.right) and \
            (posy >= self.array.rect.top and posy <= self.array.rect.bottom):
                print "selected"
                return True
        return False

    def is_active(self):
        return self.array.checked()

    def activate(self, bool):
        if not bool:
            self.array.check()

