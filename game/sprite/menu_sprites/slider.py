# -*- coding: utf-8 -*-

from game.sprite.miSprite import MiSprite
from game.gestorRecursos import GestorRecursos
from game.settings.settings import Settings

slider = "menus/slider.png"
bar = "menus/bar.png"

#----------------------------------------------#

class Bar(MiSprite):
    def __init__ (self, surface, (x,y)):
        MiSprite.__init__(self);
        self.image = surface
        self.rect = self.image.fill(Settings().getParam("RED"))
        self.position = (x,y)

    #sobreescribimos
    def establecerPosicion(self, posicion):
        self.posicion = posicion
        self.rect.left = self.posicion[0]
        self.rect.top = self.posicion[1]

#----------------------------------------------#

class Slider(MiSprite):
    def __init__ (self, surface, (x,y)):
        MiSprite.__init__(self);
        self.image = surface
        self.rect = self.image.fill(Settings().getParam("ORANGE"))
        self.position = (x,y)

    #sobreescribimos
    def establecerPosicion(self, (posx,posy)):
        self.posicion = posx
        self.rect.left = posx
        self.rect.top = posy

    def mover(self, x):
        self.posicion = x
        self.rect.left = x

#----------------------------------------------#

class SliderBar(object):
    #cualquier/todo personaje del juego
    def __init__ (self, x, y, font):
        self.active = False
        self.value = 0
        self.screen_width = Settings().getParam("SCREEN_WIDTH")
        self.screen_height = Settings().getParam("SCREEN_HEIGHT")
        width = 40
        height = 10
        x1 = x * 0.01 * self.screen_width
        y2 = x * 0.01 * self.screen_height
        x2 = (x+width) * 0.01 * self.screen_width
        y2 = (y+height) * 0.01 * self.screen_height

        slider = Surface((x2-x1,(y2-y1)/3))
        bar = Surface(((x2-x1)/8,(y2-y1)/2))

        self.slider = Slider(slider, (x1,y1 + (length/2)))
        self.bar = Bar(bar, (x2-(slider.width/2)-(bar.width/2),(y2-y1)/3))

    def establecerPosicion(self, position):
        self.bar_position = (self.bar.rect.top,self.bar.rect.bottom,self.bar.rect.left,self.bar.rect.right)
        self.barra_recorrible = (self.bar_position[3]-self.slider.rect.width) - self.bar_position[2]

        posy = self.bar_position[0] - (self.bar_position[1] - self.bar_position[0])/3
        posx = int((self.bar_position[3] + self.bar_position[2])/2)

        self.slider.establecerPosicion((posx,posy))
        
    def move(self, posicion):
        if self.active and not self.is_mouse_selection(posicion):
            #si se mueve dentro de la barra
            if posicion[0] >= self.bar_position[2] and posicion[0]+self.slider.rect.width <= self.bar_position[3]:
                self.slider.mover(posicion[0])
            #si se mueve demasiado a la izquierda
            elif posicion[0] < self.bar_position[2]:
                self.slider.mover(self.bar_position[2])
            #si se  mueve demasiado a la derecha
            elif posicion[0]+self.slider.rect.width > self.bar_position[3]:
                self.slider.mover(self.bar_position[3]-self.slider.rect.width)
        self.posicion_to_value()

    def is_mouse_selection(self, (posx, posy)):
        if (posx >= self.slider.rect.left and posx <= self.slider.rect.right) and \
            (posy >= self.slider.rect.top and posy <= self.slider.rect.bottom):
                return True
        return False

    def is_active(self):
        return self.active

    def activate(self, bool):
        self.active = True
        
    def dibujar(self,screen):
        screen.blit_abs(self.bar.image, self.bar.rect.topleft)
        screen.blit_abs(self.slider.image, self.slider.rect.topleft)

    #------------------------------------------------------------#

    def devolverValor(self):
        return self.value

    def posicion_to_value(self):
        posicion = self.slider.rect.left - self.bar_position[2]
        self.value = posicion*100 / self.barra_recorrible
        print self.value