# -*- coding: utf-8 -*-

import math
from game.sprite.personajes import *

"Este módulo incluye todos los tipos de enemigos que heredan de la clase Enemigo"

VEL_PATRULLA = 0.15
VEL_PERSECUCION = 0.52

class Serpiente(Enemigo):
    def __init__(self):
        ESTADOS = {'ARRIBA':0,'ABAJO':1,'IZQUIERDA':2,'DERECHA':3}
        self.velOriginal = 0.25
        self.retardoOriginal = 5
        self.serpiente = None
        Enemigo.__init__(self,'personajes/king_cobra.png','personajes/king_cobra_coords.txt', [3, 3, 3, 3], self.retardoOriginal, ESTADOS, 20, 10)
        
        dir_inicial = self.rand.generarNumRand(len(ESTADOS)-1)
        if dir_inicial == self.estados['ARRIBA']:
            self.movimiento = (0,-1)
        elif dir_inicial == self.estados['ABAJO']:
            self.movimiento = (0,1)
        elif dir_inicial == self.estados['IZQUIERDA']:
            self.movimiento = (-1,0)
        elif dir_inicial == self.estados['DERECHA']:
            self.movimiento = (1,0)
        self.actualizarPostura(dir_inicial)
        self.velocidad = (self.velOriginal*self.movimiento[0], self.velOriginal*self.movimiento[1])

    def mover_cpu(self, jugador, current_room):
        if self.retardoInmunidad > 0:
            self.retardoInmunidad -= 1
            self.velocidad = (0,0)
        else:
	        mirandoPrevia = self.estado
	        
	        if self.collision:
	            if self.estado == self.estados['ARRIBA']:
	                self.movimiento = (0,1)
	                self.estado = self.estados['ABAJO']
	            elif self.estado == self.estados['ABAJO']:
	                self.movimiento = (0,-1)
	                self.estado = self.estados['ARRIBA']
	            elif self.estado == self.estados['IZQUIERDA']:
	                self.movimiento = (1,0)
	                self.estado = self.estados['DERECHA']
	            elif self.estado == self.estados['DERECHA']:
	                self.movimiento = (-1,0)
	                self.estado = self.estados['IZQUIERDA']

	        self.actualizarPostura(self.estado)
	        self.velocidad = (self.velOriginal*self.movimiento[0], self.velOriginal*self.movimiento[1])

class Patrullante(Enemigo):
    def __init__(self, imagen, cords, array_cords, retardo, vida, ataque, velOriginal):
        ESTADOS = {'ABAJO':0,'IZQUIERDA':1,'DERECHA':2,'ARRIBA':3}
        self.retardoOriginal = retardo
        self.velOriginal = velOriginal
        Enemigo.__init__(self, imagen, cords, array_cords, retardo, ESTADOS, vida, ataque);
        
        dir_inicial = self.rand.generarNumRand(len(self.estados)-1)
        if dir_inicial == self.estados['ARRIBA']:
            self.movimiento = (0,-1)
        elif dir_inicial == self.estados['ABAJO']:
            self.movimiento = (0,1)
        elif dir_inicial == self.estados['IZQUIERDA']:
            self.movimiento = (-1,0)
        elif dir_inicial == self.estados['DERECHA']:
            self.movimiento = (1,0)

        self.actualizarPostura(dir_inicial)
        self.velocidad = (self.velOriginal*self.movimiento[0], self.velOriginal*self.movimiento[1])
        self.siguiendo = False

    def patrulla(self, jugador):
        if self.collision:
            if self.estado == self.estados['ARRIBA']:
                self.movimiento = (0,1)
                self.estado = self.estados['ABAJO']
            elif self.estado == self.estados['ABAJO']:
                self.movimiento = (0,-1)
                self.estado = self.estados['ARRIBA']
            elif self.estado == self.estados['IZQUIERDA']:
                self.movimiento = (1,0)
                self.estado = self.estados['DERECHA']
            elif self.estado == self.estados['DERECHA']:
                self.movimiento = (-1,0)
                self.estado = self.estados['IZQUIERDA']
        
        self.velocidad = (self.velOriginal*self.movimiento[0], self.velOriginal*self.movimiento[1])
        self.actualizarPostura(self.estado)

    def seguir(self, jugador):
        self.incrementoX, self.incrementoY = self.detect_jugador(jugador)
        self.velocidad = (self.incrementoX * self.velOriginal, self.incrementoY * self.velOriginal)

class Guardia_Prox(Patrullante):
    def __init__(self):
        Patrullante.__init__(self,'personajes/shiva.png','personajes/shiva.coords', [4, 4, 4, 4], 5, 30, 2, 2)

    def mover_cpu(self, jugador, current_room):
        if self.retardoInmunidad > 0:
            self.retardoInmunidad -= 1
            self.velocidad = (0,0)
        else:
            posiX, posiY = jugador.rect.center
            miX, miY = self.rect.center
            distancia = math.sqrt(pow(abs(miX-posiX),2) + pow(abs(miY-posiY),2))
            
            if distancia <= 400:
                self.velOriginal = VEL_PATRULLA*2
                self.siguiendo = True
            else:
                self.velOriginal = VEL_PATRULLA
                self.siguiendo = False
                
            if self.siguiendo:
                self.seguir(jugador)
            else:
                self.patrulla(jugador)
        
class Guardia_Vel(Patrullante):
    def __init__(self):
        Patrullante.__init__(self,'personajes/ifrit.png','personajes/ifrit.coords', [4, 4, 4, 4], 5, 30, 10, 1)
        self.hitbox = self.rect.inflate((-60,-60))

    def mover_cpu(self, jugador, current_room):
        if self.retardoInmunidad > 0:
            self.retardoInmunidad -= 1
            self.velocidad = (0,0)
        else:
            mirandoPrevia = self.estado

            if jugador.running and jugador.movimiento != (0,0):
                self.velOriginal = VEL_PERSECUCION
                self.seguir(jugador)
            else:
                self.velOriginal = VEL_PATRULLA
                self.patrulla(jugador)

class Ammyt(Patrullante):
    def __init__(self):
        Patrullante.__init__(self,'personajes/ammyt.png','personajes/ammyt_coords.txt', [4, 4, 4, 4], 5, 100, 15, 0.5)
        dir_inicial = self.estados['IZQUIERDA']
        self.actualizarPostura(dir_inicial)
        self.movimiento = (0,0)
        self.estados_jugador = None
        self.posicion_original = (0,0)
        self.yendo = False

    def establecerPosicion(self, posicion):
        self.posicion_original = posicion
        self.rect.center = posicion
        if self.hitbox:
            self.hitbox.center = self.rect.center # la alineamos al centro
            self.hitbox.bottom = self.rect.bottom # y lo pegamos a los "pies" del sprite

    def conflicto(self, jugador):
        self.yendo = False
        if jugador.atacando and self.retardoInmunidad <= 0:
            self.quitarVida(jugador.ataque)
            self.retardoInmunidad = self.retardoInmunidadOriginal

    def mover_cpu(self, jugador, current_room):
        if self.retardoInmunidad > 0:
            self.retardoInmunidad -= 1
            self.velocidad = (0,0)
        else:
            if self.estados_jugador == None:
                self.estados_jugador = jugador.ESTADOS

            if jugador.estado == self.estados_jugador["IZQUIERDA"]:
                #ignorarlo
                pass
            else:
                posicion_relativa = self.detect_jugador(jugador)
                if self.yendo:
                    self.velocidad = (posicion_relativa[0] * self.velOriginal, posicion_relativa[1] * self.velOriginal)
                else:
                    if self.rect.center != self.posicion_original:
                        self.velocidad = (-posicion_relativa[0] * self.velOriginal, -posicion_relativa[1] * self.velOriginal)
                    else:
                        self.yendo = True

