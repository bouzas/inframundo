# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *
#from game.settings.settings import Setting
#from settings.settings import Settings

TEXT_LIMIT = 50
vowels = "aeiuoAEIOU"
white = (255,255,255)
black = (0,0,0)

class Dialogo():
        
        def __init__ (self, screen, mixer, coordenadas,texto):
                self.coordenadas = coordenadas
                self.screen = screen
                self.indice = 0
                self.texto = ""
                self.textoCompleto = ""
                if(len(texto)>TEXT_LIMIT): self.textoCompleto = texto[:TEXT_LIMIT]+"..."
                else: self.textoCompleto = texto
                self.palabras = self.textoCompleto.split(" ")
                self.limiteArray = len(self.palabras)
                self.mixer = mixer
                
        def read(self):
                if(self.indice < self.limiteArray):
                        nuevaPalabra = self.palabras[self.indice]
                        self.texto += nuevaPalabra+" "
                        pitidos = 0
                        for letter in nuevaPalabra:
                                if letter in vowels:
                                        pitidos +=1
                        self.indice += 1
                        self.mixer.playSound("beep.ogg", 0, None, pitidos, 76, 0)
                        print self.texto, pitidos, len(self.texto), self.limiteArray, self.indice
                        return self.texto
                else:
                        print self.texto, len(self.texto), self.limiteArray, self.indice
                        return self.texto
                
        def isReaded(self):
                return self.palabras == self.palabras[0:self.indice]

        def rapidRead(self):
                self.texto = self.textoCompleto
                self.indice = self.limiteArray
                print self.texto

        def addRect(self):
                self.rect = pygame.draw.rect(self.screen, (Settings().getParam("BLACK")), self.coordenadas, 2)
                pygame.display.update()

        def addText(self):
                self.screen.blit(self.font.render(self.read(), True, Settings().getParam("WHITE")), (200, 100))
                pygame.display.update()

        def dibujar(self, screen):
                self.addRect()
                self.addText()
