# -*- coding: utf-8 -*-

from game.sprite.personajes import *
import game.sprite.base
from game.sound import MyMixer
# Clase Protagonista

class Protagonista(Personaje):
	def __init__(self):
		self.ESTADOS = {'ARRIBA':0,'ABAJO':1,'IZQUIERDA':2,'DERECHA':3,
		'ATK_ARRIBA':4, 'ATK_IZQUIERDA':5, 'ATK_ABAJO':6, 'ATK_DERECHA':7,
		'MUERTE':8}
		self.retardoRapido = 1
		self.retardoNormal = 3
		self.retardoLento = 7
		Personaje.__init__(self, "personajes/marlon_grande.png", "personajes/marlon_grande_coords.txt", [9,9,9,9,6,6,6,6,6], self.retardoNormal, self.ESTADOS, 100, 10)
		self.velocidadNormal = 0.4
		self.velocidadRapida = 0.9
		self.retardoInmunidad = 250
		self.retardoInmune = 0
		self.retardoAtaque = 0
		self.staminaBase = 100
		self.stamina = 100
		self.inmune = False
		self.atacando = False
		self.running = False
		self.tired = False
		self.attack_box = self.rect.copy()
		self.attack_box.size = (self.rect.width*1.5, self.rect.height*1.5)
		self.attack_box.center = self.rect.center

		self.hitbox = self.rect.copy()
		self.hitbox.size = (self.hitbox.width * 0.75, self.hitbox.height * 0.75) # calculamos un hitbox que sea un 75% del sprite
		self.hitbox.center = self.rect.center # alineamos al centro
		self.hitbox.bottom = self.rect.bottom # y lo pegamos a los "pies" del sprite

		self.osiris_parts = []

		self.mixer = MyMixer()
		self.sonidoGolpe = self.mixer.getSound("punch.wav")
		self.sonidoAtaque = self.mixer.getSound("ataque.wav")
		self.sonidoCaminarNormal = self.mixer.getSound("walkRegular.wav")
                self.sonidoCaminarRapido = self.mixer.getSound("walkFast.wav")

	def update(self, args):
		tiempo, grupoParedes = args
		direccionPrevia = self.estado
		dirY,dirX = self.movimiento
		diagonal = 0.707

		if self.retardoAtaque > 0: 
			self.retardoAtaque -= 1
		else:
			self.atacando = False

		if not self.atacando:
		#Indicamos en qué dirección mira el personaje según los valores de la tupla movimiento
			if dirX != 0 or dirY != 0:
				if abs(dirX) >= abs(dirY):
					self.estado = self.estados['DERECHA'] if dirX > 0 else self.estados['IZQUIERDA']
				else:
					self.estado = self.estados['ABAJO'] if dirY > 0 else self.estados['ARRIBA']
			else:
				self.numImagenPostura = len(self.coordenadasHoja[self.estado])
		
		elif self.estado < 4:
			self.estado += 4
			self.numImagenPostura =	len(self.coordenadasHoja[self.estado])	

			# Si el sprite se mueve en diagonal, ajustamos la velocidad proporcionalmente
		if (dirX and dirY):
			dirX *= diagonal
			dirY *= diagonal

		# Mover al personaje
		self.velocidad = (0,0)
		if dirX != 0 or dirY != 0:
			if self.running and self.stamina > 0 and not self.tired:
				self.velocidad = (dirX * self.velocidadRapida, dirY * self.velocidadRapida)
				self.stamina -= 1
			elif self.stamina <= 0:
				self.tired = True
				self.velocidad = (dirX * self.velocidadNormal, dirY * self.velocidadNormal)
			elif (self.retardoInmune<=self.retardoInmunidad/2):
				self.velocidad = (dirX * self.velocidadNormal, dirY * self.velocidadNormal)

		if self.atacando:
			self.stamina -= 1
			if self.stamina <= 0:
				self.stamina = 0
				self.tired = True
				self.velocidad = (dirX * self.velocidadNormal, dirY * self.velocidadNormal)
		elif self.stamina < self.staminaBase and not self.running:
			self.stamina += 1
			if self.stamina >= self.staminaBase:
				self.stamina = 100
				self.tired = False

		if self.atacando:
			self.establecerRetardo(self.retardoNormal)
		elif self.running:
			self.establecerRetardo(self.retardoRapido)
		elif self.tired:
			self.establecerRetardo(self.retardoNormal)
		else:
			self.establecerRetardo(self.retardoNormal)

		self.actualizarPostura(self.estado)
		# restar tiempo inmunidad
		self.retardoInmune -= tiempo
		
		super(Protagonista, self).update(args)

	def mover (self, teclasPulsadas):
		# Indicamos la acción a realizar segun la tecla pulsada para el jugador
		movimiento = []

		if teclasPulsadas[Settings().getParam("TECLA_ATAQUE")]:
			if not self.atacando and not self.tired:
                                self.sonidoAtaque.play()
				self.movimiento = (0,0)
				self.atacando = True
				self.retardoAtaque = 20


		for i in Settings().getParam("TECLAS_MOVIMIENTO"):
			movimiento.append(teclasPulsadas[i])

		if teclasPulsadas[K_LSHIFT] and not self.tired:
			self.running = True
		elif not teclasPulsadas[K_LSHIFT]:
			self.running = False

		#asignamos valores al movimiento en función de las teclas pulsadas
		arriba = -1 if movimiento[0] else 0
		abajo = 1 if movimiento[1] else 0
		izquierda = -1 if movimiento[2] else 0
		derecha = 1 if movimiento[3] else 0
		self.movimiento = (arriba + abajo, izquierda + derecha)

	
	def inmunidad(self):
		if not self.retardoInmune:
			self.retardoInmune -= 1

	def rebotar(self):
		if self.estado == self.estados['ABAJO']:
			#saltar arriba
			self.establecerPosicion((self.posicion[0],self.posicion[1]-REBOTE))
		elif self.estado == self.estados['ARRIBA']:
			#saltar abajo
			self.establecerPosicion((self.posicion[0],self.posicion[1]+REBOTE))
		if self.estado == self.estados['DERECHA']:
			#saltar izquierda
			self.establecerPosicion((self.posicion[0]-REBOTE,self.posicion[1]))
		elif self.estado == self.estados['IZQUIERDA']:
			#saltar derecha
			self.establecerPosicion((self.posicion[0]+REBOTE,self.posicion[1]))	

	def quitarVida(self, cantidad):
		if self.retardoInmune<=0 and not self.atacando:
                        self.sonidoGolpe.play()
			self.vida -= cantidad
			if self.vida <= 0:
				self.dead = True
			else:
				self.retardoInmune = self.retardoInmunidad
			
		return self.dead

	def get_osiris_parts_collected(self):
		return self.osiris_parts

	def update_items(self, item):
		name = item.__class__.__name__
		if name == "ItemAtaque":
			self.ataque += item.ataque
		elif name == "OsirisPiernas":
			self.osiris_parts.append(item.image)
		elif name == "OsirisTorso":
			self.osiris_parts.append(item.image)
		elif name == "OsirisCabeza":
			self.osiris_parts.append(item.image)

	def update_attack_box(self):
		if self.estado == self.estados['ATK_ABAJO']:
			self.attack_box.topleft = (self.rect.left,self.rect.center[1])
		elif self.estado == self.estados['ATK_ARRIBA']:
			self.attack_box.bottomleft = (self.rect.left,self.rect.center[1])
		elif self.estado == self.estados['ATK_DERECHA']:
			self.attack_box.topleft = (self.rect.center[0], self.rect.top)
		elif self.estado == self.estados['ATK_IZQUIERDA']:
			self.attack_box.topright = (self.rect.center[0], self.rect.top)	

def collided_attack(sprite, jugador):
	rect = sprite.rect
	jugador.update_attack_box()
	return jugador.attack_box.colliderect(rect)
