# -*- coding: utf-8 -*-

import base, json, pygame

_JSON_FILE = "recursos/plantillas/sprites/estaticos.json"
class Estatico(base.Animado):

	def __init__(self, args, pos):
		archivoImagen = args[0]
		archivoCoordenadas = args[1]
		numImagenes = args[2]
		retardoAnimacion = args[3]
		estados = args[4]
		super(Estatico, self).__init__(archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados)

		self.solido = args[5]
		self.rect.center = pos

_staticDict = {}

def createByNameInRoom(name, habitacion, tile=None):
	pos = habitacion.tile(tile).center
	return Estatico(_staticDict[name], pos)

def createByName(name, pos):
	return Estatico(_staticDict[name], pos)

_staticDict = json.load(open(_JSON_FILE))

