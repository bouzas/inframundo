# -*- coding: utf-8 -*-

from game.sprite.base import *
from game.gestorRecursos import GestorRecursos
from game.settings import *


PARTES = {'CABEZA':0,'CUERPO':1,'PIERNAS':2}

class Item(Animado):
    def __init__(self, archivoImagen, archivoCoords, numImages, retardoAnimacion, estados):
        Animado.__init__(self, archivoImagen, archivoCoords, numImages, retardoAnimacion, estados)
    
class Osiris(Item):
    def __init__(self, parte):
        Item.__init__(self, 'personajes/sansiris.png', 'personajes/sansiris_coords.txt', [16, 4, 2], 5, PARTES);
        super(Osiris, self).actualizarPostura(parte)
        self.estado = parte
        
class OsirisCabeza(Osiris):
    def __init__(self):
        Osiris.__init__(self, PARTES["CABEZA"])
    
    def actualizarPostura(self):
        super(OsirisCabeza, self).actualizarPostura(PARTES["CABEZA"])

class OsirisTorso(Osiris):
    def __init__(self):
        Osiris.__init__(self, PARTES["CUERPO"])
    
    def actualizarPostura(self):
        super(OsirisTorso, self).actualizarPostura(PARTES["CUERPO"])

class OsirisPiernas(Osiris):
    def __init__(self):
        Osiris.__init__(self, PARTES["PIERNAS"])
    
    def actualizarPostura(self):
        super(OsirisPiernas, self).actualizarPostura(PARTES["PIERNAS"])

class ItemAtaque(Item):
    def __init__(self):
        ESTADOS = {'ROJO':0,'VIOLETA':1,'VERDE':2,'AZUL':3}
        Item.__init__(self, 'estaticos/potion.png', 'estaticos/potion_coords.txt', [5, 5, 5, 5], 5, ESTADOS);
        super(ItemAtaque, self).actualizarPostura(self.estados["VIOLETA"])
        self.estado = self.estados['VIOLETA']
        self.ataque = 5
    
    def actualizarPostura(self):
        super(ItemAtaque, self).actualizarPostura(self.estados["VIOLETA"])

    