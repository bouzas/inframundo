# -*- coding: utf-8 -*-

import pygame
import game.util as util
from pygame.locals import *
from game.gestorRecursos import GestorRecursos

class Animado(pygame.sprite.Sprite):

	def __init__ (self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados):
		pygame.sprite.Sprite.__init__(self);
		
		#Cargamos hoja de sprites
		self.hoja = GestorRecursos.CargarImagen(archivoImagen, -1)
		self.hoja = self.hoja.convert_alpha()
		self.estado = 0
		self.estados = estados # diccionario de estados (posturas o cualquier otra cosa que signifique una animación diferente)
		self.hitbox = None # no inicializar la hitbox en base, hacerlo solo para los sprites que lo necesiten
		self.retardoAnimacion = retardoAnimacion
		self.retardoMovimiento = retardoAnimacion

		#Leer coordenadas de los sprites de un archivo de texto
		if(archivoCoordenadas):
			datos = GestorRecursos.CargarArchivoCoordenadas(archivoCoordenadas)
			datos = datos.split()
			self.numImagenPostura = 0;
			cont = 0;
			self.coordenadasHoja = [];
			#buscamos las posiciones en cada una de las cuatro direcciones
			for linea in range(len(estados)):
				self.coordenadasHoja.append([])
				tmp = self.coordenadasHoja[linea]
				for postura in range(1, numImagenes[linea]+1):
					tmp.append(pygame.Rect((int(datos[cont]), int(datos[cont+1])), (int(datos[cont+2]), int(datos[cont+3]))))
					cont += 4

			# El rectangulo del Sprite
			self.rect = pygame.Rect(50,50,self.coordenadasHoja[self.estado][self.numImagenPostura][2],self.coordenadasHoja[self.estado][self.numImagenPostura][3])

			self.image = self.hoja.subsurface(self.coordenadasHoja[self.estado][self.numImagenPostura])
		else:
			self.image = self.hoja

	def actualizarPostura(self, estado):
		# Miramos si ha pasado el retardo para dibujar una nueva postura
		# o si se ha producido un cambio en el estado del sprite
		if ((self.retardoMovimiento < 0) or estado != self.estado):
			self.retardoMovimiento = self.retardoAnimacion
			self.estado = estado
			# Actualizamos la postura
			self.numImagenPostura += 1
			if self.numImagenPostura >= len(self.coordenadasHoja[self.estado]):
				self.numImagenPostura = 0
			#if self.numImagenPostura < 0:
			#	self.numImagenPostura = len(self.coordenadasHoja[self.estado])-1

			# Cambiamos la imagen en función de a dónde mire y el numero de postura
			self.image = self.hoja.subsurface(self.coordenadasHoja[self.estado][self.numImagenPostura])
		elif self.retardoAnimacion:
			self.retardoMovimiento -= 1

	def establecerPosicion(self, posicion):
		self.rect.center = posicion
		if self.hitbox:
			self.hitbox.center = self.rect.center # la alineamos al centro
			self.hitbox.bottom = self.rect.bottom # y lo pegamos a los "pies" del sprite

	def establecerRetardo(self, retardo):
		self.retardoAnimacion = retardo

class Movil(Animado):
	def __init__(self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados):
		Animado.__init__(self, archivoImagen, archivoCoordenadas, numImagenes, retardoAnimacion, estados)
		self.velocidad = (0,0)
		self.collision = False # sirve para informar de si el sprite chocó en este tick
		self.solido = True # los sprites moviles pueden ser sólidos, los sólidos no atraviesan las paredes / otros sprites

	def incrementarPosicion(self, incremento):
		(incrementox, incrementoy) = incremento
		self.establecerPosicion((self.rect.center[0]+incrementox, self.rect.center[1]+incrementoy))

	def update(self, args):
		def resolverColisiones(eje): # X es cero (0), Y es uno (1)
			collision = False
			""" busca colisiones en el eje indicado """
			if self.solido: # si es sólido
				obstaculos = pygame.sprite.spritecollide(self, grupoParedes, False, util.collided) # buscamos colisiones
				collision = len(obstaculos) > 0
				if collision: # ha habido colision
					maxSep = 0
					for obstaculo in obstaculos:
						sepx, sepy = util.calcMinSeparation(self, obstaculo, (velx, 0)[eje], (0, vely)[eje]) # ojo a como se selecciona si la velocidad es 0 o vel en función del eje
						maxSep = max(maxSep, sepx, sepy) # no importa el eje, uno de ellos siempre será cero
					if self.velocidad[eje] > 0: maxSep *= -1 # la función devuelve un valor absoluto que tenemos que corregir
					correccion = (maxSep * (1-eje), maxSep * eje) # cágate con la legibilidad
					self.incrementarPosicion(correccion) # y empujamos el sprite fuera de colisión usando esa distancia
			return collision

		self.collision = False
		tiempo, grupoParedes = args
		velx, vely = self.velocidad
		incrementox = velx*tiempo
		incrementoy = vely*tiempo

		self.incrementarPosicion((incrementox, 0)) # movemos el sprite en el eje horizontal
		self.collision |= resolverColisiones(0) # resolvemos colisiones en x

		self.incrementarPosicion((0, incrementoy)) # movemos el sprite en el eje vertical
		self.collision |= resolverColisiones(1) # resolvemos colisiones en y
