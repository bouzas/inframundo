# -*- encoding: utf-8 -*-

# Wrapper para el escalado automático de la pantalla

import pygame
from pygame.locals import *
from settings.settings import *

FULLSIZE = FULLSIZEX, FULLSIZEY = 1920,1080

class Scrollable(pygame.Surface):

    def __init__(self, disp):
        super(Scrollable, self).__init__(FULLSIZE)
        self.disp = disp if disp else pygame.Surface(FULLSIZE)
        self.setup()
    
    def setup(self):
        screen_width = Settings().getParam("SCREEN_WIDTH")
        screen_height = Settings().getParam("SCREEN_HEIGHT")
        #screen_width, screen_height = self.disp.get_rect().size
        xfactor, yfactor = screen_width/float(FULLSIZEX), screen_height/float(FULLSIZEY)
        self.factor = xfactor if xfactor > yfactor else yfactor
        self.scale = self.scalex, self.scaley = int(FULLSIZEX * self.factor), int(FULLSIZEY * self.factor)
        
        self.alignment = alix, aliy = ((self.scalex-screen_width)/2, (self.scaley-screen_height)/2)
        self.viewport = pygame.Rect((alix,aliy),(screen_width,screen_height))
        # Coordenadas del scroll
        self.scroll_reset()

    def _make_rel(self, pos):
        if type(pos) == pygame.Rect: # pos can either be a rect or a tuple
            posx, posy = pos.topleft
        else:
            posx, posy = pos
        
        scrollx, scrolly = self.scrollCoords
        alix, aliy = self.alignment
        relpos = (posx-scrollx+(self.get_rect().width/2),posy-scrolly+(self.get_rect().height/2))
        return relpos

    def blit(self, img, pos, area=None):
        relpos = self._make_rel(pos)
        return super(Scrollable, self).blit(img, relpos, area)
   
    def _make_abs(self, img, pos, side="bottomleft"):
        if type(pos) == pygame.Rect: # pos can either be a rect or a tuple
            posx, posy = pos.topleft
        else:
            posx, posy = pos
        screenx, screeny = getattr(self.disp.get_rect(), side)
        imgw, imgh = img.get_rect().size
        alix, aliy = self.alignment
        
        if side == "bottomleft":
            abspos = posx+alix, -posy-imgh+(screeny+aliy)/self.factor
        elif side == "topright":
            abspos = -posx-imgw+(screenx+alix)/self.factor, posy+aliy
        elif side == "bottomright":
            abspos = -posx-imgw+(screenx+alix)/self.factor, -posy-imgh+(screeny+aliy)/self.factor
        else:
            abspos = posx+alix, posy+aliy
        return abspos
 
    # Blitea una imagen en un punto fijo de la pantalla respecto a una esquina concreta de la pantalla
    # Usando el argumento opcional side se puede elegir una de las esquinas
    # El argumento pos se entiende como distancia a esa esquina hacia el interior de la pantalla
    def blit_abs(self, img, pos, area=None, side="bottomleft"):
        abspos = self._make_abs(img,pos,side)
        return super(Scrollable, self).blit(img, abspos, area)

    # Se pasa unas coordenadas de centro y el scroll se centra en ese punto del nivel
    def scroll(self, center):
        self.scrollCoords = center
    
    def scroll_reset(self):
        self.scrollCoords = self.get_rect().center

""" Screen es una clase wrapper para pygame.display que soporta scroll y autoescalado
en función de la resolución objetivo. Tiene un método adicional blit_abs que permite
blitear en puntos fijos de la pantalla y otro scroll que permite definir qué punto
estará en el centro de la pantalla al pintar """
class Screen(Scrollable):

    def __init__(self):
        screen_width = Settings().getParam("SCREEN_WIDTH")
        screen_height = Settings().getParam("SCREEN_HEIGHT")
        surface = pygame.display.set_mode((screen_width, screen_height),0,32)
        super(Screen, self).__init__(surface)
    
    # Añadimos un flip que realice el escalado automáticamente
    def flip(self):
        scaled = pygame.transform.smoothscale(self, self.scale) # create a scaled copy
        
        alix, aliy = self.alignment
        self.disp.blit(scaled, (0,0), area=self.viewport) # blit viewport area of scaled image to display
        pygame.display.flip() # flip buffer
        self.fill((0,0,0)) # and clear our screen
        pygame.draw.rect(pygame.display.get_surface(),(0,0,0),pygame.display.get_surface().get_rect()) # and clear fb
    
# Wrapper sobre los métodos de pygame.mouse para adaptarlos a la multireshulacion
class Mouse():

    def _map(self,tup):
        x,y = tup
        alix, aliy = display.alignment
        return (int(x-alix * display.factor), int(y-aliy * display.factor))

    def _unmap(self,tup,rel=False):
        x,y = tup
        if rel:
            alix, aliy = 0,0
        else:
            alix, aliy = display.alignment
        return (int(x / display.factor)+alix, int(y / display.factor)+aliy)
    
    def get_pressed(self):
        return pygame.mouse.get_pressed()
        
    def get_pos(self):
        return self._unmap(pygame.mouse.get_pos())
        
    def get_rel(self):
        return self._unmap(pygame.mouse.get_rel(),rel=True)
        
    def set_pos(self,tup):
        return pygame.mouse.set_pos(_map(tup))
    
    def set_visible(self,visible):
        return pygame.mouse.set_visible(visible)
    
    def get_focused(self):
        return pygame.mouse.get_focused()
    
    def set_cursor(self, size, hotspot, xormasks, andmasks):
        return pygame.mouse.set_cursor(size, hotspot, xormasks, andmasks)
    
    def get_cursor(self, size, hotspot, xormasks, andmasks):
        return pygame.mouse.set_cursor(size, hotspot, xormasks, andmasks)

display = Screen()
mouse = Mouse()
