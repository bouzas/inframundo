# -*- encoding: utf-8 -*-

import pygame
from pygame.locals import *

pygame.font.init()

MAIN_FONT_FILENAME = "recursos/fuentes/aescrawl.ttf"
SUBTITLE_FONT_FILENAME = "recursos/fuentes/Cryptik.ttf"
TITLE_FONT_FILENAME = "recursos/fuentes/FFF_Tusj.ttf"

TEXT_TYPES = [
	pygame.font.Font(None, 10), 
	pygame.font.Font(TITLE_FONT_FILENAME, 150), 
	pygame.font.Font(MAIN_FONT_FILENAME, 100), 
	pygame.font.Font(MAIN_FONT_FILENAME, 50),
	pygame.font.Font(MAIN_FONT_FILENAME, 30),
	pygame.font.Font(SUBTITLE_FONT_FILENAME, 180)
]