{
"SCREEN_HEIGHT": 800,
"DIRSOUND": "recursos/sonidos/",
"TECLAS_MOVIMIENTO": [119, 115, 97, 100],
"TECLA_ATAQUE": 32,
"TECLA_INTERACT": 101,
"SCREEN_WIDTH": 1280,
"FRAMERATE": 60,
"DIRMUSIC": "recursos/musica/",
"VOLUME": 0.5,
"ORANGE": [255, 153, 51],
"RESOURCE_PATH": "recursos",
"BLACK": [0, 0, 0],
"YELLOW": [255, 255, 0],
"BLACK": [0, 0, 0],
"WHITE": [255, 255, 255],
"GREEN": [0, 255, 0],
"RED": [255, 0, 0]
}
