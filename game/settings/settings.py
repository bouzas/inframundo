# -*- encoding: utf-8 -*-
import json
import pygame
from pygame.locals import *

paramsFile='game/settings/settings.txt'
#paramsFile='settings.txt'


class Settings:
    class __Settings:
        def __init__(self):
            with open(paramsFile) as inFile:
                self.data = json.load(inFile)
                
        def __str__(self):
            return repr(self) + self.data
    instance = None
    def __init__(self):
        if not Settings.instance:
            Settings.instance = Settings.__Settings()
        else:
            with open(paramsFile) as inFile:
                Settings.instance.data = json.load(inFile)
    def __getattr__(self, name):
        return getattr(self.instance, name)

    def getParam(self, param):
        if param in self.instance.data:
            return self.instance.data[param]

    def setParam(self, param, value):
        self.instance.data[param] = value
        with open(paramsFile, 'wb') as outFile:
            json.dump(self.instance.data, outFile)
