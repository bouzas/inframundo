# -*- encoding: utf-8 -*-

# Wrapper para el escalado automático de la pantalla

import pygame
from pygame.locals import *
from settings.settings import Settings

class MyMixer():
    
    def __init__(self):
        pygame.mixer.pre_init(44100, 16, 2, 4096)
        pygame.mixer.init()

    #devuelve un objeto sonido
    def getSound(self,sound):
        return pygame.mixer.Sound(Settings().getParam("DIRSOUND")+sound)
    
    #reproducir directamente
    # vol es el volumen sumado (o restado) al volumen actual
    # pos es para hacer sonar en alguna posicion del mapa, se pasa su posicion en el eje horizontal
    # veces es para repetir el numero de veces deseado y -1 para en bucle (no recomendable) para eso mejor bajarse el sonido y modificarlo con set sound
    # maxtime es el tiempo en milisegundo que se reproducira
    # fade es para establecer cuanto tiempo le toma el sonido en tomar su volumen
    
    def playSound(self,sound, vol=0, veces=0, maxtime=0, fade=0):
        if(veces>0): veces += -1
        sound = pygame.mixer.Sound(Settings().getParam("DIRSOUND")+sound)
        channel = sound.play(veces, maxtime, fade)
        if (Settings().getParam("VOLUME")+vol>1):
            vol=1
        elif (Settings().getParam("VOLUME")+vol<0):
            vol=0
        else:
            vol = Settings().getParam("VOLUME")+vol
        channel.set_volume(vol)
        return channel

    #cambiar posicion de sonido en reproduccion
    def setSound(self, channel, vol, pos):
        pos= float(pos)/Settings().getParam("ORIGINAL_WIDTH")
        if (Settings().getParam("VOLUME")+vol>1):
            vol=1
        elif (Settings().getParam("VOLUME")+vol<0):
            vol=0
        else:
            vol = Settings().getParam("VOLUME")+vol
        return channel.set_volume(vol*(1-pos),vol*pos)

    #pausar todo
    def pauseAll(self):
        pygame.mixer.pause()
        pygame.mixer.music.pause()

    def unpauseAll(self):
        pygame.mixer.unpause()
        pygame.mixer.music.unpause()

    #reproducir directamente
    def playMusic(self,music):
        pygame.mixer.music.load(Settings().getParam("DIRMUSIC")+music)
        pygame.mixer.music.play(-1)
    
    def stopMusic(self):
        pygame.mixer.music.stop()

    def pauseMusic(self):
        pygame.mixer.music.pause()

    def unpauseMusic(self):
        pygame.mixer.music.unpause()
 

