# -*- coding: utf-8 -*-

import numpy
import pygame.surfarray

def toGreyscale(surf):
    """ Hace una copia en escala de grises de una imagen """
    tmp = pygame.surfarray.array3d(surf)
    tmp[:,:,0] = tmp[:,:,0] * 0.2126
    tmp[:,:,1] = tmp[:,:,1] * 0.7152
    tmp[:,:,2] = tmp[:,:,2] * 0.0722
    gs = numpy.sum(tmp, axis=2)
    gs3d = numpy.repeat(gs[:, :, numpy.newaxis], 3, axis=2) # aparentemente, make_surface con un array 2d no produce un greyscale si no un mapeado de color así que hay que hacerlo A MANO... ._.
    return pygame.surfarray.make_surface(gs3d)

def calcMinSeparation(spr1, spr2, velx, vely):
    """ Calcula cuánto se ha colado un sprite dentro de otro,
    para obtener cada eje independientemente basta con pasar
    0 a la velocidad del otro eje """
    rect1 = spr1.hitbox if hasattr(spr1, "hitbox") and spr1.hitbox != None else spr1.rect
    rect2 = spr2.hitbox if hasattr(spr2, "hitbox") and spr2.hitbox != None else spr2.rect

    sepx, sepy = 0,0
    if abs(vely) > 0:
        if vely > 0: # bajando
            sepy = rect1.bottom - rect2.top
        else: # subiendo
            sepy = rect2.bottom - rect1.top
    if abs(velx) > 0:
        if velx > 0: # derecha
            sepx = rect1.right - rect2.left
        else: # izquierda
            sepx = rect2.right - rect1.left
    return sepx, sepy

def collided(sprite1, sprite2):
    """ Una función para comprobar colisiones empleando el hitbox si lo hubiere """
    if (hasattr(sprite1, "solido") and not sprite1.solido) or (hasattr(sprite2, "solido") and not sprite2.solido): return False

    rect1 = sprite1.hitbox if hasattr(sprite1, "hitbox") and sprite1.hitbox != None else sprite1.rect
    rect2 = sprite2.hitbox if hasattr(sprite2, "hitbox") and sprite2.hitbox != None else sprite2.rect

    return rect1.colliderect(rect2)
