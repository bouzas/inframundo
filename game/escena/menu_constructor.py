# -*- encoding: utf-8 -*-

import json, sys
from game.settings.text_settings import *
from game.gestorRecursos import GestorRecursos
import game.escena.menu_functions as menu_functions
from game.settings.settings import Settings
from game.screen import *

MENU_CONFIG_PATH = 'recursos/plantillas/menus/'

class MenuLabel(): 
	def __init__(self, text, x, y, textFont, color):
		self.text = text
		self.textFont = textFont
		self.label = textFont.render(text, 1, color)
		rect = self.label.get_rect()
		self.position = (x-(rect.width*0.5),y-(rect.height*0.5))

	def mark(self, mark, color):
		self.textFont.set_italic(mark)
		self.label = self.textFont.render(self.text, 1, color)

	def is_selected(self, mpos, absrect):
		x, y = mpos
		return absrect.collidepoint(mpos)

class MenuItem(MenuLabel):
	def __init__(self, text, x, y, typeText, colorOff, function, colorOn=None):
		MenuLabel.__init__(self, text, x, y, typeText, colorOff)
		self.function = function
		self.colorOn = colorOn
		self.colorOff = colorOff

class MenuObject():
	def __init__(self, color, backgroundIMG, caption, labels, items):
		self.color = color
		self.backgroundIMG = GestorRecursos.CargarImagen(backgroundIMG) if backgroundIMG != None else None
		self.caption = caption
		self.labels = labels
		self.items = items

def open_json(filename):
	BLACK = [0,0,0]
	ORANGE = [255, 153, 51]
	WHITE = [255, 255, 255]
	
	try:
		with open(MENU_CONFIG_PATH + filename + '.json' ) as data_file:    
			data = json.load(data_file)

		bg_color = None
		color = BLACK
		backgroundIMG, caption, text, function = None, None, None, None
		labels, items = [], []
		x, y, numType, textFont = 0, 0, 0, None
		width = FULLSIZEX
		height = FULLSIZEY

		if 'bg_color' in data:
			if data['bg_color'] != "":
				bg_color = locals()[ str(data['bg_color']) ]

		if 'backgroundIMG' in data:
			backgroundIMG = data['backgroundIMG']

		if 'caption' in data:
			caption = data['caption']

		if 'labels' in data:
			for label in data['labels']:
				x = (label['x'] * 0.01) * width
				y = (label['y'] * 0.01) * height
				text = label['text']
				color = locals()[ str(label['color']) ]
				if 'type' in label:
					numType = label['type']
					textFont = TEXT_TYPES[numType]
				labels.append(MenuLabel(text, x, y, textFont, color))

		if 'items' in data:
			for item in data['items']:
				x = (item['x'] * 0.01) * width
				y = (item['y'] * 0.01) * height
				text = item['text']
				colorOn = locals()[ str(item['colorOn']) ]
				colorOff = locals()[ str(item['colorOff']) ]
				if 'type' in item:
					numType = item['type']
					textFont = TEXT_TYPES[numType]
				function = getattr(menu_functions, item['function'])
				items.append(MenuItem(text, x, y, textFont, colorOff, function, colorOn))

		if 'menu_objects' in data:
			objeto = None
			for item in data['menu_objects']:
				objeto = reduce(getattr, item["class"].split("."), sys.modules[__name__])

		return MenuObject(bg_color, backgroundIMG, caption, labels, items)

	except IOError as e:
		print 'I/O error({0}): {1}'.format(e.errno, e.strerror)
		raise
	except ValueError:
		print 'Could not convert data to an integer.'
		raise
	except NameError: 
		raise
	except:
		print 'Unexpected error:', sys.exc_info()[0]
		raise
