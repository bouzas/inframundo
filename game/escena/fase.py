# -*- coding: utf-8 -*-

import pygame, game.util, random
import game.escena.mapas.mapas as mapas
import game.escena.mapas.habitacion as hab
import game.escena.mapas.nivel as niv
from game.escena.mapas.constantes import *
from game.sprite.protagonista import *
from game.sprite.estaticos import *
from game.escena.escena import Escena
from game.sprite.hud import Hud
from game.sprite.items import ItemAtaque
import game.screen as screen
from game.sprite.enemigos2 import Ammyt
from game.sound import MyMixer

class Fase(Escena):

    ROOM_AMOUNT = [5,7,11,15,19]

    def __init__(self, director,num_fase,jugador=None, hud = None): # recordad que las fases se empiezan por 0
    	pygame.display.set_caption("Inframundo: la Duat")
        Escena.__init__(self, director)
        self.num_fase = num_fase
        
        # Si es la primera fase, reseedeamos el generador
        if self.num_fase == 0:
            mapas.generator.reseed()

        # Creamos un nivel vacío
        self.nivel = mapas.generator.generarNivel(Fase.ROOM_AMOUNT[num_fase] if num_fase < len(Fase.ROOM_AMOUNT) else 30)
        self.habitaciones = self.nivel.get_habitaciones()

        # Creamos el sprite del protagonista
        self.jugador = jugador if jugador else Protagonista()
        self.grupoJugador = pygame.sprite.Group(self.jugador)
        self.hud = Hud(self.jugador, self.num_fase)

        # Ponemos a los jugadores en sus posiciones iniciales
        self.jugador.establecerPosicion(self.nivel.inicio.rect.center) # self.nivel.inicio es la habitación de inicio del nivel

        self.grupoDecorados = pygame.sprite.Group()
        self.grupoEstaticos = pygame.sprite.Group()
        self.grupoTriggers = pygame.sprite.Group()
        for h in self.habitaciones:
            self.grupoDecorados.add(h) # habitaciones
            self.grupoEstaticos.add(h.puertas) # puertas, rocas
            self.grupoTriggers.add(h.triggers) # triggers

        # Grupo con objetos no atravesables
        self.grupoInquebrantables = pygame.sprite.Group()
        for p in self.nivel.get_paredes():
            self.grupoInquebrantables.add(p)

        # Añadimos los estáticos a estáticos e inquebrantables
        for h in self.habitaciones:
            if h and h.mapa:
                for r in h.mapa:
                    for t in r:
                        if t:
                            self.grupoEstaticos.add(t)
                            self.grupoInquebrantables.add(t)

        self.grupoEnemigos = pygame.sprite.Group()
        mapas.generator.generarEnemigos(self.nivel)
        self.grupoVida = mapas.generator.generarVidas(self.nivel)
        
        # Creamos in grupo para items
        self.grupoItems = pygame.sprite.Group()
        # Creamos las partes de Osiris
        self.osiris = mapas.generator.generarOsiris(self.nivel)
        self.grupoItems.add(self.osiris)
        
        # Creamos un grupo con los Sprites que se mueven: protagonista, enemigos, proyectiles...
        self.grupoSpritesDinamicos = pygame.sprite.Group()
        self.grupoSpritesDinamicos.add(self.jugador)
        self.grupoSpritesDinamicos.add(self.grupoEnemigos.sprites())

        self.current_room = pygame.sprite.spritecollideany(self.jugador, self.habitaciones)

        self.mixer = MyMixer()
        self.music = MUSICA_ENTORNO[self.nivel.entorno]
        self.mixer.playMusic(self.music)
    
    def update(self, tiempo):
        from game.escena.menu import EmptyMenu

        previous_room = self.current_room
        self.current_room = pygame.sprite.spritecollideany(self.jugador, self.habitaciones)
        if previous_room != self.current_room:
            if previous_room.enemigos != None:
                previous_room.enemigos = self.grupoEnemigos.copy()
                self.grupoSpritesDinamicos.remove(previous_room.enemigos.sprites())
            self.grupoEnemigos.empty()
            if self.current_room.enemigos != None:
                self.grupoEnemigos = self.current_room.enemigos.copy()
                self.grupoSpritesDinamicos.add(self.grupoEnemigos.sprites())

        # Primero, se mueven los enemigos según sus propios métodos y lógicas internas
        for enemigo in self.grupoEnemigos.sprites():
            enemigo.mover_cpu(self.jugador, self.current_room)

        # Actualizamos todos los Sprites dinámicos invocando su método update
        self.grupoSpritesDinamicos.update((tiempo, self.grupoInquebrantables))

        # Colisiones entre jugador y triggers ANTES que perder vida, seamos misericordiosos
        trigger = pygame.sprite.spritecollideany(self.jugador, self.grupoTriggers, util.collided)
        if trigger:
            trigger.trigger(self, self.jugador, self.interact)

        # Comprobamos si hay hay colisiones entre el jugador y los enemigos
        
        if not self.jugador.atacando:
            conflictos = pygame.sprite.groupcollide(self.grupoEnemigos, self.grupoJugador, False, False, util.collided)
            if conflictos != {}:
                if self.jugador.quitarVida(conflictos.keys()[0].ataque):
                    self.director.cambiarEscena( EmptyMenu(self.director, "game_over_menu") )
                for enemigo in conflictos.keys():
                    enemigo.conflicto(self.jugador)
        else:
            conflictos = pygame.sprite.groupcollide(self.grupoEnemigos, self.grupoJugador, False, False, collided_attack)
            if conflictos != {}:
                for enemigo in conflictos.keys():
                    enemigo.conflicto(self.jugador)
                    if enemigo.dead:
                        enemigo.kill()
                        item = enemigo.recompensa()
                        if item:
                            self.grupoItems.add(item)

        # Comprobamos si hay hay colisiones entre el jugador y las fuentes de vida
        conflictos = pygame.sprite.groupcollide(self.grupoVida, self.grupoJugador, False, False)
        if conflictos != {}:
            for vida in self.grupoVida:
                if vida.rect.colliderect(self.jugador) and self.interact:
                    vida.curarVida(self.jugador)

        # actualizamos animacion items
        for item in self.grupoItems:
            if self.jugador.rect.colliderect(item):
                self.jugador.update_items(item)
                item.kill()
            else:
                item.actualizarPostura()
        
        for est in self.grupoEstaticos:
            est.actualizarPostura(0) # Sigh........ SIN MÁS 

        self.hud.update(tiempo)

    def dibujar(self, pantalla):
        # Dibujamos, por orden el decorado y los sprites
        pantalla.scroll(self.jugador.rect.center) # siempre antes de empezar a pintar
        self.grupoDecorados.draw(pantalla)
        self.grupoEstaticos.draw(pantalla)
        self.grupoSpritesDinamicos.draw(pantalla)
        self.grupoVida.draw(pantalla)
        self.grupoItems.draw(pantalla)
        self.hud.dibujar(pantalla)

    def eventos(self, lista_eventos):
        from game.escena.menu import Menu
        self.interact = False
        # Miramos a ver si hay algun evento de salir del programa
        for evento in lista_eventos:
            if evento.type == pygame.QUIT:
                self.director.salirPrograma()
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE: # pressing escape quits
                    snap = self.snapshot() # haz un fotograma
                    self.mixer.pauseMusic()
                    greyscale = game.util.toGreyscale(snap) # to greyscale
                    pause = Menu(self.director, "pause_menu")
                    pause.content.backgroundIMG = greyscale
                    self.director.apilarEscena( pause )
                self.interact = evento.key == Settings().getParam("TECLA_INTERACT") # el boolean interact se activa si se ha recibido pulsación de interact este turno

        # Indicamos la acción a realizar segun la tecla pulsada para cada jugador
        teclasPulsadas = pygame.key.get_pressed()

        self.jugador.mover(teclasPulsadas)
    
    def next_level(self):
        if self.num_fase < 4:
        	next = Fase(self.director, self.num_fase+1, self.jugador)
        else:
            next = FaseFinal(self.director, self.num_fase+1, self.jugador)
        self.director.cambiarEscena(next)

    def snapshot(self):
        surf = screen.Scrollable(None) # creamos una scrollable sin nada
        surf.scroll(self.jugador.rect.center)
        self.grupoDecorados.draw(surf)
        self.grupoEstaticos.draw(surf)
        self.grupoSpritesDinamicos.draw(surf)
        return surf

class FaseFinal(Escena):
    def __init__(self, director, num_fase, jugador=None):
        Escena.__init__(self, director)
        self.num_fase = num_fase

        # Creamos un nivel vacío
        self.nivel = niv.Nivel(2, ENTORNO_TEMPLO)
        self.nivel.habitaciones = [[None for x in range(ANCHO_NIVEL)] for x in range(ALTO_NIVEL)]
        self.nivel.habitaciones[0][0] = hab.HabitacionInicio(self.nivel, (0,0))
        self.nivel.inicial = self.nivel.habitaciones[0][0]

        # código para añadir el orbe de sanación a la habitación
        self.nivel.habitaciones[0][1] = hab.HabitacionEspecial(self.nivel, (1,0))

        self.habitaciones = self.nivel.get_habitaciones()
        for habitacion in self.habitaciones:
            habitacion.crearPuertas()

        self.grupoEnemigos = pygame.sprite.Group()
        self.enemigo = Ammyt()
        self.enemigo.establecerPosicion((self.habitaciones[1].rect.center[0],self.habitaciones[1].rect.center[1]))
        self.grupoEnemigos.add(self.enemigo)

        # Creamos el sprite del protagonista
        self.jugador = jugador if jugador else Protagonista()
        self.jugador.establecerPosicion(self.nivel.inicial.rect.center) # self.nivel.inicio es la habitación de inicio del nivel
        self.grupoJugador = pygame.sprite.Group(self.jugador)
        self.hud = Hud(self.jugador, self.num_fase)

        # Ponemos a los jugadores en sus posiciones iniciales
        self.grupoDecorados = pygame.sprite.Group()
        self.grupoEstaticos = pygame.sprite.Group()
        self.grupoTriggers = pygame.sprite.Group()
        for h in self.habitaciones:
            self.grupoDecorados.add(h) # habitaciones
            self.grupoEstaticos.add(h.puertas) # puertas, rocas
            self.grupoTriggers.add(h.triggers) # triggers

        # Grupo con objetos no atravesables
        self.grupoInquebrantables = pygame.sprite.Group()
        for p in self.nivel.get_paredes():
            self.grupoInquebrantables.add(p)

        # Añadimos los estáticos a estáticos e inquebrantables
        for h in self.habitaciones:
            if h and h.mapa:
                for r in h.mapa:
                    for t in r:
                        if t:
                            self.grupoEstaticos.add(t)
                            self.grupoInquebrantables.add(t)

        vida = CuracionTotal()
        vida.establecerPosicion((self.nivel.inicial.rect.center[0],self.nivel.inicial.rect.top+(self.nivel.inicial.rect.center[1]/2)))
        self.grupoVida = pygame.sprite.Group()
        self.grupoVida.add(vida)
        
        self.grupoEstaticos.add(self.grupoVida.sprites())
 
        # Creamos un grupo con los Sprites que se mueven: protagonista, enemigos, proyectiles...
        self.grupoSpritesDinamicos = pygame.sprite.Group()
        self.grupoSpritesDinamicos.add(self.jugador)
        self.grupoSpritesDinamicos.add(self.enemigo)

    def update(self, tiempo):
        from game.escena.menu import EmptyMenuFunction
        
        current_room = pygame.sprite.spritecollideany(self.jugador, self.habitaciones)
        if current_room == self.habitaciones[1]:
            self.enemigo.mover_cpu(self.jugador, current_room)

        # Actualizamos todos los Sprites dinámicos invocando su método update
        self.grupoSpritesDinamicos.update((tiempo, self.grupoInquebrantables))

        # Colisiones entre jugador y triggers ANTES que perder vida, seamos misericordiosos
        trigger = pygame.sprite.spritecollideany(self.jugador, self.grupoTriggers, util.collided)
        if trigger:
            trigger.trigger(self, self.jugador, self.interact)

        # Comprobamos si hay hay colisiones entre el jugador y los enemigos
        
        if not self.jugador.atacando:
            conflictos = pygame.sprite.groupcollide(self.grupoEnemigos, self.grupoJugador, False, False, util.collided)
            if conflictos != {}:
                if self.jugador.quitarVida(conflictos.keys()[0].ataque):
                    self.director.cambiarEscena( EmptyMenu(self.director, "game_over_menu") )
                for enemigo in conflictos.keys():
                    enemigo.conflicto(self.jugador)
        else:
            conflictos = pygame.sprite.groupcollide(self.grupoEnemigos, self.grupoJugador, False, False, collided_attack)
            if conflictos != {} and self.enemigo in conflictos:
                    self.enemigo.quitarVida(self.jugador.ataque)
                    self.enemigo.conflicto(self.jugador)
                    if self.enemigo.dead:
                        self.enemigo.kill()
                        self.director.cambiarEscena(EmptyMenuFunction(self.director,"victory_menu"))

        # Comprobamos si hay hay colisiones entre el jugador y las fuentes de vida
        conflictos = pygame.sprite.groupcollide(self.grupoVida, self.grupoJugador, False, False)
        if conflictos != {}:
            for vida in self.grupoVida:
                if vida.rect.colliderect(self.jugador) and self.interact:
                    vida.curarVida(self.jugador)

        self.hud.update(tiempo)

    def eventos(self, lista_eventos):
        from game.escena.menu import Menu
        self.interact = False
        # Miramos a ver si hay algun evento de salir del programa
        for evento in lista_eventos:
            if evento.type == pygame.QUIT:
                self.director.salirPrograma()
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE: # pressing escape quits
                    snap = self.snapshot() # haz un fotograma
                    greyscale = game.util.toGreyscale(snap) # to greyscale
                    pause = Menu(self.director, "pause_menu")
                    pause.content.backgroundIMG = greyscale
                    self.director.apilarEscena( pause )
                self.interact = evento.key == Settings().getParam("TECLA_INTERACT") # el boolean interact se activa si se ha recibido pulsación de interact este turno

        # Indicamos la acción a realizar segun la tecla pulsada para cada jugador
        teclasPulsadas = pygame.key.get_pressed()

        self.jugador.mover(teclasPulsadas)

    def dibujar(self, pantalla):
        # Dibujamos, por orden el decorado y los sprites
        pantalla.scroll(self.jugador.rect.center) # siempre antes de empezar a pintar
        self.grupoDecorados.draw(pantalla)
        self.grupoEstaticos.draw(pantalla)
        self.grupoSpritesDinamicos.draw(pantalla)
        self.hud.dibujar(pantalla)

    def snapshot(self):
        surf = screen.Scrollable(None) # creamos una scrollable sin nada
        surf.scroll(self.jugador.rect.center)
        self.grupoDecorados.draw(surf)
        self.grupoEstaticos.draw(surf)
        self.grupoSpritesDinamicos.draw(surf)
        return surf
