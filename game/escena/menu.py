# -*- encoding: utf-8 -*-

from collections import OrderedDict
from game.escena.fase import *
from game.gestorRecursos import *
import game.escena.menu_constructor as constructor
import game.escena.menu_functions
from game.sound import MyMixer

#--------------------------------------------------------------------------------#

class Menu(Escena):

	def __init__(self, director, name):
		self.content = constructor.open_json(name)
		self.director = director
		self.mpos = 0
		self.mouse_is_visible = False
		self.cur_item = 0
		pygame.display.set_caption(self.content.caption)
		if len(self.content.items) != 0:
			self.content.items[self.cur_item].mark(True, self.content.items[self.cur_item].colorOn)
		# musica
                self.mixer = MyMixer()
                self.plantilla = name
                if (self.plantilla == "pause_menu"): self.mixer.pauseMusic()
                elif (self.plantilla == "main_menu"):
                        self.music = "main.ogg"
                        self.mixer.playMusic(self.music)
                elif (self.plantilla == "game_over_menu"): self.mixer.stopMusic()
                
	def update(self, *args):
		self.mpos = constructor.mouse.get_pos()

	def eventos(self, lista_eventos):
		# Se mira si se quiere salir de esta escena
		for event in lista_eventos:
			if event.type == pygame.QUIT: # si estás aquí para poner de vuelta el binding de ESC para cerrar el juego sin más desde cualquier menú, que sepas que te encontraré en el log de git y te quemaré la casa
				terminate(self.director)
			if event.type == pygame.K_ESCAPE:
				back(self.director)
			if event.type == pygame.KEYDOWN:
				self.mouse_is_visible = False
				self.set_keyboard_selection(event.key)
			if event.type == pygame.MOUSEBUTTONDOWN:
				for item in self.content.items:
					abspos = constructor.display._make_abs(item.label, item.position, side="topleft")
					absrect = pygame.Rect(abspos, item.label.get_rect().size)
					if item.is_selected(self.mpos, absrect):
                                                if (self.plantilla == "pause_menu"): self.mixer.pauseMusic()
                                                elif (self.plantilla == "main_menu"): self.mixer.stopMusic()
                                                else: print self.plantilla
						item.function(self.director)

	def dibujar(self, pantalla):
		if constructor.mouse.get_rel() != (0, 0):
			self.mouse_is_visible = True
			self.cur_item = 0

		self.set_mouse_visibility()

		# Redraw the background
		if self.content.color != None:
			pantalla.fill(self.content.color)
		if self.content.backgroundIMG != None:
			pantalla.blit(self.content.backgroundIMG, (0,0))

		for item in self.content.labels:
			pantalla.blit_abs(item.label, item.position, None, 'topleft')

		for item in self.content.items:
			if self.mouse_is_visible:
				self.set_mouse_selection(item)
			pantalla.blit_abs(item.label, item.position, None, 'topleft')
 
	#-------Métodos propios del menú--------#

	def set_mouse_visibility(self):
		constructor.mouse.set_visible(self.mouse_is_visible)
 
	def set_keyboard_selection(self, key):
		for item in self.content.items: # Return all to neutral
			self.content.items[self.cur_item].mark(False, item.colorOff)

		if (key == pygame.K_UP or key == pygame.K_w) and \
				self.cur_item > 0:
			self.cur_item -= 1
		elif (key == pygame.K_UP or key == pygame.K_w) and \
				self.cur_item <= 0:
			self.cur_item = len(self.content.items) - 1
		elif (key == pygame.K_DOWN or key == pygame.K_s) and \
				self.cur_item < len(self.content.items) - 1:
			self.cur_item += 1
		elif (key == pygame.K_DOWN or key == pygame.K_s) and \
				self.cur_item == len(self.content.items) - 1:
			self.cur_item = 0
 
		self.content.items[self.cur_item].mark(True, self.content.items[self.cur_item].colorOn)
 
		# Finally check if Enter or Space is pressed
		if key == pygame.K_SPACE or key == pygame.K_RETURN:
			self.content.items[self.cur_item].function(self.director)
 
	# Marks the MenuItem the mouse cursor hovers on. #
	def set_mouse_selection(self, item):
		abspos = constructor.display._make_abs(item.label, item.position, side="topleft")
		absrect = pygame.Rect(abspos, item.label.get_rect().size)
		if item.is_selected(self.mpos, absrect):
			item.mark(True, item.colorOn)
		else:
			item.mark(False, item.colorOff)

	def salirPrograma(self):
		self.director.salirPrograma()  

class EmptyMenu(Menu):

	def __init__(self, director, name):
		Menu.__init__(self,director,name)

	def eventos(self, lista_eventos):
		# Se mira si se quiere salir de esta escena
		for event in lista_eventos:
			if event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
				self.director.salirEscena()

	def dibujar(self, pantalla):
		# Redraw the background
		pantalla.fill(self.content.color)
		
		if self.content.backgroundIMG != None:
			pantalla.blit(self.content.backgroundIMG, (0,0))

		for item in self.content.labels:
			pantalla.blit_abs(item.label, item.position, None, 'topleft')

class EmptyMenuFunction(Menu):

	def __init__(self, director, name):
		Menu.__init__(self,director,name)

	def eventos(self, lista_eventos):
		# Se mira si se quiere salir de esta escena
		for event in lista_eventos:
			if event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
				self.director.cambiarEscena(EmptyMenu(self.director, "credits"))

	def dibujar(self, pantalla):
		# Redraw the background
		pantalla.fill(self.content.color)
		
		if self.content.backgroundIMG != None:
			pantalla.blit(self.content.backgroundIMG, (0,0))

		for item in self.content.labels:
			pantalla.blit_abs(item.label, item.position, None, 'topleft')
