# -*- coding: utf-8 -*-

from game.sprite.enemigos import *
from game.sprite.enemigos2 import *
from game.sprite.vida import *
from game.sprite.items import *

# Ruta de las imágenes
IMG_PATH = "habitaciones"
ENTORNO_PATH = ["/cueva","/templo","/jungla","/rio","/fuego"]

# Ancho y alto de la cuadrícula de habitaciones
ANCHO_NIVEL   = 10
ALTO_NIVEL    = 5

# Enumeración de los entornos
ENTORNO_CUEVA   = 0
ENTORNO_TEMPLO  = 1
ENTORNO_JUNGLA  = 2
ENTORNO_RIO     = 3
ENTORNO_FUEGO   = 4
ENTORNO_MAX = 4

# Enumeración de los tipos de habitación
HAB_INICIO  = 0
HAB_NORMAL  = 1
HAB_MEJORA  = 2
HAB_DIOS    = 3
HAB_ESFINGE = 4
HAB_ESPECIAL = 5
TIPO_FILENAME = ["/inicio.png", "/normal.png", "/mejora.png", "/dios.png", "/esfinge.png", "/sala_osiris.png"]

FORMA_RECT = 0 
FORMA_CRUZ = 1
WALLS_FILENAME = ["/rectangular.walls", "/cruz.walls"]

# Dimensiones de las habitaciones
ANCHO_HABIT = 13
ALTO_HABIT  = 7
LADO_TILE = 115

NORTE = 0
SUR = 1
ESTE = 2
OESTE = 3

PROP_SYMBOLS = {
' ': None,
'r':'roca',
'f':'fuego'
}

ENEMY_SYMBOLS = {
    "b" : Murcielago,   
    "t" : Thief,
    "s" : Serpiente,
    "v" : Guardia_Vel,
    "p" : Guardia_Prox
}

VIDA_SYMBOLS = {
    "p" : CuracionParcial,
    "t" : CuracionTotal
}

OSIRIS_SYMBOLS = {
    "c" : OsirisCabeza,
    "t" : OsirisTorso,
    "p" : OsirisPiernas
}

MUSICA_ENTORNO = ["cueva.ogg","templo.ogg","jungla.ogg","rio.ogg","fuego.ogg"]