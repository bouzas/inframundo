# -*- coding: utf-8 -*-

import pygame
from game.gestorRecursos import GestorRecursos
import game.sprite.estaticos as estaticos
import game.sprite.trigger as trig
import game.escena.mapas.templates as templates
from game.escena.mapas.constantes import *

# Clase abstracta que contiene los datos de las casillas que forman una habitación
class Habitacion(pygame.sprite.Sprite):

    def __init__(self, nivel, posicion):
        super(Habitacion, self).__init__()
        self.posx, self.posy = posicion # esta posicion se refiere a los índices en la cuadrícula de habitaciones
        self.tipo = None # tipo de habitacion
        self.mapa = None # mapa de la habitación
        self.nivel = nivel # instancia del nivel en el que está ubicada
        self.forma = FORMA_RECT # TODO: Más formas
        self.paredes = [] # lista de sprites de las paredes del nivel
        self.puertas = []
        self.triggers = []
        self.enemigos = None
    
    def setup_position(self):
        self.rect.top = self.posy * (1035 + 30)
        self.rect.left = self.posx * (1725 + 30)

    def getFreeSpace(self):
        """ Devuelve una lista de tuplas con los huecos vacios de la habitación """
        return [(x,y) for y in range(ALTO_HABIT) for x in range(ANCHO_HABIT) if not self.mapa[y][x]]
    
    def crearPuertas(self):
        def crearPuerta(pos, rotation) :
            puerta = estaticos.createByName("puerta", pos)
            puerta.image = pygame.transform.rotate(puerta.image, rotation)
            return puerta
        
        def obtenerDestino(direccion):
            if direccion == NORTE:
                return self.nivel.habitaciones[self.posy-1][self.posx].exitTile(SUR).center
            elif direccion == SUR:
                return self.nivel.habitaciones[self.posy+1][self.posx].exitTile(NORTE).center
            elif direccion == ESTE:
                return self.nivel.habitaciones[self.posy][self.posx+1].exitTile(OESTE).center
            elif direccion == OESTE:
                return self.nivel.habitaciones[self.posy][self.posx-1].exitTile(ESTE).center
            else:
                return None
        
        if self.tieneNorte() :
            x,y = self.rect.midtop
            puerta = crearPuerta(self.rect.midtop, 0)
            puerta.rect.bottom = y+115
            self.puertas.append(puerta)
            teleport = trig.Teleporter(self.exitTile(NORTE))
            teleport.dest = obtenerDestino(NORTE)
            teleport.facing = [0,4] # NOTA: 0 es el estado ARRIBA del jugador, 4 el ATK_ARRIBA, esta es la lista de estados aceptables para atravesar el portal
            self.triggers.append(teleport)
        if self.tieneSur() :
            x,y = self.rect.midbottom
            puerta = crearPuerta(self.rect.midbottom, 180)
            puerta.rect.top = y-115
            self.puertas.append(puerta)
            teleport = trig.Teleporter(self.exitTile(SUR))
            teleport.dest = obtenerDestino(SUR)
            teleport.facing = [1,6] # ABAJO, ATK_ABAJO
            self.triggers.append(teleport)
        if self.tieneOeste() :
            x,y = self.rect.midleft
            puerta = crearPuerta(self.rect.midleft, 90)
            puerta.rect.right = x+90
            self.puertas.append(puerta)
            teleport = trig.Teleporter(self.exitTile(OESTE))
            teleport.dest = obtenerDestino(OESTE)
            teleport.facing = [2,5] # IZQUIERDA, ATK_IZQUIERDA
            self.triggers.append(teleport)
        if self.tieneEste() :
            x,y = self.rect.midright
            puerta = crearPuerta(self.rect.midright, 270)
            puerta.rect.left = x-115
            self.puertas.append(puerta)
            teleport = trig.Teleporter(self.exitTile(ESTE))
            teleport.dest = obtenerDestino(ESTE)
            teleport.facing = [3,7] # DERECHA, ATK_DERECHA
            self.triggers.append(teleport)
    
    def exitTile(self, direccion):
        """Devuelve el tile de una salida, las salidas son NORTE, SUR, ESTE y OESTE"""
        if direccion == NORTE:
            return self.tile((6,0))
        elif direccion == SUR:
            return self.tile((6,6))
        elif direccion == ESTE:
            return self.tile((12,3))
        elif direccion == OESTE:
            return self.tile((0,3))
    
    # Método abstracto para generar una habitación, toma como argumento un generador de números aleatorios.
    def generar(self, rand):
        self.mapa = [[None for x in range(ANCHO_HABIT)] for y in range(ALTO_HABIT)]
    
    def _tieneSalida(self,x,y):
        if 0 <= self.posx + x < ANCHO_NIVEL and 0 <= self.posy + y < ALTO_NIVEL:
            return bool(self.nivel.habitaciones[self.posy+y][self.posx+x])
    def tieneNorte(self):
        return self._tieneSalida(0,-1)
    def tieneSur(self):
        return self._tieneSalida(0,1)
    def tieneEste(self):
        return self._tieneSalida(1,0)
    def tieneOeste(self):
        return self._tieneSalida(-1,0)

    def tile(self, tile):
        """ Devuelve un rectángulo representando un tile de esta habitación con las coordenadas
        dadas, que deben estar entre 0 y ANCHO_HABIT-1 y 0 y ALTO_HABIT-1 para x e y respectivamente """
        xtile, ytile = tile
        return pygame.Rect((self.rect.left + LADO_TILE*(xtile+1), self.rect.top + LADO_TILE*(ytile+1)), (LADO_TILE,LADO_TILE))
    
    def carga_paredes(self, filename):
        #Leer coordenadas de las paredes de un archivo de texto
        datos = GestorRecursos.CargarArchivoCoordenadas(IMG_PATH + WALLS_FILENAME[self.forma]).split()
        cont = 0
        #leemos las coordenadas x,y,w,h de las paredes
        for pared in range(4):
            spr = pygame.sprite.Sprite()
            spr.rect = pygame.Rect((int(datos[cont]), int(datos[cont+1])), (int(datos[cont+2]), int(datos[cont+3])))
            spr.rect.move_ip(self.rect.left, self.rect.top)
            spr.image = pygame.Surface((0,0))
            self.paredes.append(spr)
            cont += 4

# Implementaciones de los tipos de habitacion
class HabitacionInicio(Habitacion):
    def __init__(self, nivel, posicion):
        super(HabitacionInicio, self).__init__(nivel, posicion)
        self.tipo = HAB_INICIO
        self.image = GestorRecursos.CargarImagen(IMG_PATH + ENTORNO_PATH[nivel.entorno] + TIPO_FILENAME[self.tipo])
        self.rect = self.image.get_rect()
        self.setup_position()
        self.carga_paredes(WALLS_FILENAME[self.forma])
    
    def generar(self, rand):
        # TODO: Método para generar habitaciones de inicio.
        super(HabitacionInicio, self).generar(rand)

class HabitacionNormal(Habitacion):
    def __init__(self, nivel, posicion):
        super(HabitacionNormal, self).__init__(nivel, posicion)
        self.tipo = HAB_NORMAL
        self.image = GestorRecursos.CargarImagen(IMG_PATH + ENTORNO_PATH[nivel.entorno] + TIPO_FILENAME[self.tipo])
        self.rect = self.image.get_rect()
        self.setup_position()
        self.carga_paredes(WALLS_FILENAME[self.forma])

    def generar(self, rand):
        super(HabitacionNormal, self).generar(rand)
        templates.poblarHabitacion(rand, self)

class HabitacionMejora(Habitacion):
    def __init__(self, nivel, posicion):
        super(HabitacionMejora, self).__init__(nivel, posicion)
        self.tipo = HAB_MEJORA
        self.image = GestorRecursos.CargarImagen(IMG_PATH + ENTORNO_PATH[nivel.entorno] + TIPO_FILENAME[self.tipo])
        self.rect = self.image.get_rect()
        self.setup_position()
        self.carga_paredes(WALLS_FILENAME[self.forma])

    def generar(self, rand):
        # TODO: Método para generar habitaciones de mejora.
        super(HabitacionMejora, self).generar(rand)

class HabitacionEsfinge(Habitacion):
    def __init__(self, nivel, posicion):
        super(HabitacionEsfinge, self).__init__(nivel, posicion)
        self.tipo = HAB_ESFINGE
        self.image = GestorRecursos.CargarImagen(IMG_PATH + ENTORNO_PATH[nivel.entorno] + TIPO_FILENAME[self.tipo])
        self.rect = self.image.get_rect()
        self.setup_position()
        self.carga_paredes(WALLS_FILENAME[self.forma])

    def generar(self, rand):
        # TODO: Método para generar habitaciones de esfinge.
        super(HabitacionEsfinge, self).generar(rand)

class HabitacionDios(Habitacion):
    def __init__(self, nivel, posicion):
        super(HabitacionDios, self).__init__(nivel, posicion)
        self.tipo = HAB_DIOS
        self.image = GestorRecursos.CargarImagen(IMG_PATH + ENTORNO_PATH[nivel.entorno] + TIPO_FILENAME[self.tipo])
        self.rect = self.image.get_rect()
        self.setup_position()
        self.carga_paredes(WALLS_FILENAME[self.forma])
    
    def generar(self, rand):
        super(HabitacionDios, self).generar(rand)
        
        centralTileXY = ctx, cty = (ANCHO_HABIT/2, ALTO_HABIT/2)
        endTile = self.tile(centralTileXY)
        self.triggers.append(trig.LevelEnd(endTile))
        self.mapa[cty][ctx] = estaticos.createByNameInRoom("vortex", self, centralTileXY)
        
class HabitacionEspecial(Habitacion):
    def __init__(self, nivel, posicion):
        super(HabitacionEspecial, self).__init__(nivel, posicion)
        self.tipo = HAB_ESPECIAL
        self.image = GestorRecursos.CargarImagen(IMG_PATH + TIPO_FILENAME[self.tipo])
        self.rect = self.image.get_rect()
        self.setup_position()
        self.carga_paredes(WALLS_FILENAME[self.forma])
    
    def generar(self, rand):
        super(HabitacionEspecial, self).generar(rand)
