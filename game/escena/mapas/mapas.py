# -*- coding: utf-8 -*-

import random, nivel, time
from game.escena.mapas.constantes import *

# Se encarga de generar niveles
class Generador(object):

    SINGLETON = None # Atributo de clase

    def __init__(self, semilla):
        self.semilla = semilla
        self.rand = random.Random(semilla) # instancia exclusiva de generador de números pseudoaleatorios para determinismo
        self.rand.seed(semilla)
        
    @staticmethod
    def instancia():
        if Generador.SINGLETON == None:
            Generador.SINGLETON = Generador(0)
        return Generador.SINGLETON

    def generarNivel(self, habitaciones):
        nuevo_nivel = nivel.Nivel(habitaciones, self.rand.randint(0, ENTORNO_MAX))
        nuevo_nivel.poblar(self.rand)
        return nuevo_nivel

    def reseed(self):
    	self.semilla = int(round(time.time() * 1000))
        self.rand.seed(self.semilla)

    def generarEnemigos(self, nivel):
        return nivel.generar_enemigos(self.rand)

    def generarOsiris(self, nivel):
        return nivel.generar_osiris(self.rand)

    def generarVidas(self, nivel):
        return nivel.generar_vidas(self.rand)

    def generarNumRand(self, max):
        return self.rand.randint(0,max)

generator = Generador.instancia() # inicializamos el generador con los milisegundos
