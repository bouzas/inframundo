# -*- coding: utf-8 -*-

import json
import game.sprite.estaticos as estaticos
from game.escena.mapas.constantes import *

_JSON_FILE_RECT_ROOM = "recursos/plantillas/habitaciones/rectangular.json"
_JSON_FILE_CRUZ_ROOM = "recursos/plantillas/habitaciones/cruz.json"
_JSON_FILE_ENEMY_GROUPS = "recursos/plantillas/sprites/enemies.json"

_rectangularTemplates = json.load(open(_JSON_FILE_RECT_ROOM))['templates']
_enemiesTemplates = json.load(open(_JSON_FILE_ENEMY_GROUPS))['templates']

def _getRandRoomTemplate(rand, shape):
    if shape==FORMA_RECT:
        return _rectangularTemplates[rand.randint(0, len(_rectangularTemplates)-1)]
    elif shape==FORMA_CRUZ:
        return None
    else:
        return None

def _generateStatics(rand, room, template):
    for y in range(len(template)):
        for x in range(len(template[y])):
            name = PROP_SYMBOLS[template[y][x]] # obtenemos el nombre del prop que hay que generar
            if name:
                room.mapa[y][x] = prop = estaticos.createByNameInRoom(name, room, (x,y))
                estado = rand.randint(0, len(prop.estados)-1)
                prop.actualizarPostura(estado)

def _getRandomEnemyGroup(rand):
    return _enemiesTemplates[rand.randint(0,len(_enemiesTemplates)-1)]

def poblarHabitacion(rand, room):
    template = _getRandRoomTemplate(rand, room.forma)
    _generateStatics(rand, room, template)
