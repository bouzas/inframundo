# -*- coding: utf-8 -*-

import pygame, habitacion as h
import templates
from constantes import *

# Contiene los datos de las habitaciones de un nivel
class Nivel(object):

    def __init__(self, numHabit, entorno):
        self.numHabit = numHabit # número de habitaciones que contendrá el mapa, sin contar BOSS y ESFINGE
        self.habitaciones = None
        self.inicio = None # El atributo inicio contiene la habitación de inicio
        self.entorno = entorno # Contiene el entorno (cueva, templo, etc) del nivel

    def get_habitaciones(self):
        result = []
        for l in self.habitaciones:
            for h in l:
                if h:
                    result.append(h) # no fui capaz de hacerlo con una list comprehension, #shame
        return result

    def get_paredes(self):
        result = []
        for l in self.habitaciones:
            for h in l:
                if h:
                    result.extend(h.paredes)
        return result

    def poblar(self, rand):
        self.habitaciones = self._generarNivel(rand)
        for l in self.habitaciones:
            for h in l:
                if h: h.crearPuertas() # generamos las puertas de las habitaciones

    # Devuelve un nivel construído sobre una malla de tamaño ANCHO x ALTO y con numHabit habitaciones.
    # El parámetro rand especifica el generador de números aleatorios que será usado.
    def _generarMapa(self, rand):
        aleatorio = [[rand.randint(0,99) for x in range(ANCHO_NIVEL)] for y in range(ALTO_NIVEL)]
        # Inicializamos la matriz de habitaciones a None
        mapa = [[False for x in range(ANCHO_NIVEL)] for y in range(ALTO_NIVEL)]
        # Elegimos un punto aleatorio para comenzar
        frontera = [(rand.randint(0, ANCHO_NIVEL-1), rand.randint(0, ALTO_NIVEL-1))]
        visitados = []
        
        h = self.numHabit
        # Hacemos un recorrido de máximo peso por el mapa hasta crear tantas habitaciones como se haya solicitado
        while(h > 0):

            # Elegimos para la iteración el último elemento de la frontera (máximo peso)
            x, y = frontera.pop()
            # Marcamos el elemento actual como habitación en el mapa
            mapa[y][x] = True
            # Y lo añadimos como visitado
            visitados.append((x,y))
            
            # Generamos los candidatos
            candidatos = [(x-1,y), (x+1,y), (x,y-1), (x,y+1)]
            # Los añadimos a la frontera (si son coordenadas válidas y no están visitados ya)
            frontera.extend([(x,y) for (x,y) in candidatos \
                if x >= 0 \
                and y >= 0 \
                and x < ANCHO_NIVEL \
                and y < ALTO_NIVEL \
                and (x,y) not in visitados \
                and (x,y) not in frontera]) # hip hip hurra! list comprehensions! :^)
            # Ordenamos la frontera por peso mínimo (los pesos se encuentran en la matriz 'aleatorio')
            frontera.sort(cmp=lambda (x1,y1),(x2,y2): aleatorio[y1][x1]-aleatorio[y2][x2]) # La función de comparación es la diferencia de los pesos en ese punto.

            # Y reducimos el número de habitaciones restantes
            h -= 1
        return mapa

    # Este método reparte las habitaciones necesarias por el nivel
    def _generarNivel(self, rand):
        mapa = self._generarMapa(rand)
        habitaciones = [[None for x in range(ANCHO_NIVEL)] for x in range(ALTO_NIVEL)]
        
        # Buscamos las habitaciones en el mapa
        indicesHab = []
        for y in range(ALTO_NIVEL):
            for x in range(ANCHO_NIVEL):
                if mapa[y][x]:
                    indicesHab.append((x,y))
        
        # Elegimos una al azar como habitación de inicio
        x,y = indicesHab.pop(rand.randint(0,len(indicesHab)-1))
        habitaciones[y][x] = h.HabitacionInicio(self, (x,y))
        self.inicio = habitaciones[y][x]
       
        # Elegimos una al azar como habitación de powerup
        x,y = indicesHab.pop(rand.randint(0,len(indicesHab)-1))
        habitaciones[y][x] = h.HabitacionMejora(self, (x,y))
        
        # Todas las demás son habitaciones estándar
        for x,y in indicesHab:
            habitaciones[y][x] = hab = h.HabitacionNormal(self, (x,y))
            hab.generar(rand)

        # Creamos un array con la cuenta de vecinos activos de cada elemento de mapa
        def contarVecinos(x,y):
            result = 0
            if x > 0        and mapa[y][x-1]:
                result += 1
            if y > 0        and mapa[y-1][x]:
                result += 1
            if x < ANCHO_NIVEL-1  and mapa[y][x+1]:
                result += 1
            if y < ALTO_NIVEL-1   and mapa[y+1][x]:
                result += 1
            return result
        vecinos = [[contarVecinos(x,y) for x in range(ANCHO_NIVEL)] for y in range(ALTO_NIVEL)]
        
        # Buscamos huecos libres con un solo vecino
        indices1Vecino = []
        for y in range(ALTO_NIVEL):
            for x in range(ANCHO_NIVEL):
                if (not mapa[y][x]) and vecinos[y][x] == 1:
                    indices1Vecino.append((x,y))

        # Elegimos uno al azar para crear la habitación del jefe
        x,y = indices1Vecino.pop(rand.randint(0,len(indices1Vecino)-1))
        habitaciones[y][x] = hab = h.HabitacionDios(self, (x,y))
        hab.generar(rand)
        return habitaciones

    def generar_enemigos(self, rand):
        #Crea el grupo de enemigos para una fase, escogiendo
        #aleatoriamente entre los grupos de enemigos de las plantillas
        habitaciones = self.get_habitaciones()

        for habitacion in self.get_habitaciones():
            if habitacion.tipo == HAB_NORMAL:
                grupoEnemigos = pygame.sprite.Group()
                group = templates._getRandomEnemyGroup(rand)
                free_space = habitacion.getFreeSpace()
                for element in group:
                    enemigo = ENEMY_SYMBOLS[element]()
                    enemigo.establecer_habitacion(habitacion)
                    tupla = free_space.pop(rand.randrange(0,len(free_space)-1))
                    tile = habitacion.tile(tupla)
                    enemigo.rect.center = tile.center
                    grupoEnemigos.add(enemigo)
                habitacion.enemigos = grupoEnemigos

    def tipo_vida(self, rand, tipo, grupoVidas, habitacion):
        free_space = habitacion.getFreeSpace()

        vida = VIDA_SYMBOLS[tipo]()
        tupla = free_space.pop(rand.randrange(0,len(free_space)-1))
        tile = habitacion.tile(tupla)
        vida.rect.center = tile.center
        grupoVidas.add(vida)
        
    
    def generar_vidas(self, rand):
        #Crea el grupo de vidas para una fase, escogiendo
        #aleatoriamente entre vida parcial o total
        #con probabilidad de 10% de total y 15% parcial
        grupoVidas = pygame.sprite.Group()
        habitaciones = self.get_habitaciones()
        paredes = self.get_paredes()

        for habitacion in self.get_habitaciones():
            if habitacion.tipo == HAB_NORMAL:
                tipo = rand.randint(0,100)
                if tipo<5:
                    self.tipo_vida(rand, "t" , grupoVidas, habitacion)
                elif tipo<20:
                    self.tipo_vida(rand, "p", grupoVidas, habitacion)

        return grupoVidas

    def generar_osiris(self, rand):
        habitaciones = self.get_habitaciones()
        for habitacion in habitaciones:
            if habitacion.tipo in [2]:
                tipo = rand.randint(0,len(OSIRIS_SYMBOLS)-1)
                if tipo==0:
                    osiris = OSIRIS_SYMBOLS["c"]()
                elif tipo==1:
                    osiris = OSIRIS_SYMBOLS["t"]()
                else:
                    osiris = OSIRIS_SYMBOLS["p"]()

                osiris.establecerPosicion(habitacion.rect.center)
                return osiris
    
    def conflicto(self, (x,y), lista_objetos):
        for ((i,j),w,h) in lista_objetos:
            if x > i and (x < i+w) and y > j and (y < j+h):
                return True
        return False

    def __str__(self):
        def simbolo(hab):
            if hab == None:
                return '. '
            elif isinstance(hab, h.HabitacionInicio):
                return '⌂ '
            elif isinstance(hab, h.HabitacionNormal):
                return '□ '
            elif isinstance(hab, h.HabitacionMejora):
                return '⚡'
            elif isinstance(hab, h.HabitacionDios):
                return '⚔ '
            elif isinstance(hab, h.HabitacionEsfinge):
                return '▲ '
            else:
                return '? '
        return ''.join([''.join([simbolo(ha) for ha in linea]) + '\n' for linea in self.habitaciones])

