# -*- encoding: utf-8 -*-

from game.director import *
from game.escena.fase import Fase, FaseFinal

def new_game(director):
	director.apilarEscena(Fase(director,0))

def load_game(director):
	director.apilarEscena(Fase(director,0))

def hello_world():
	print 'hello_world'

def opciones(director):
	from game.escena.menu import Menu
	director.apilarEscena( Menu(director, "options_menu") )

def terminate(director):
	director.salirPrograma()

def back(director):
	director.salirEscena()

def backback(director):
	director.salirEscena()
	director.salirEscena()
